import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AppSettingsService } from 'src/app/core/services/app-settings.service';
import { DataTableActionInterface } from 'src/app/modules/ida-table/ida-table.model';
import { IdaTableComponent } from 'src/app/modules/ida-table/ida-table/ida-table.component';
import { QueryParamsModel } from 'src/app/modules/ida-table/query-params.model';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  @ViewChild('idaTable', { static: true }) idaTable: IdaTableComponent;

  public displayColumns = [
    {prop: 'orgName', name: 'Organization', sortable: true, search: true },
    {prop: 'deptName', name: 'Department', sortable: true, search: true },
    { prop: 'firstName', name: 'First Name', sortable: true, search: true },
    { prop: 'lastName', name: 'LastName', sortable: true, search: true },
    { prop: 'dob', name: 'dob', sortable: true, search: true },
    { prop: 'workTitle', name: 'Work Title', sortable: true, search: true },
    { prop: 'experience', name: 'Total Experience', sortable: true, search: true },
    { prop: 'action', name: 'Action', sortable: true, show: true, cellTemplate: 'action' }
  ];
  actions: Array<DataTableActionInterface> = [];

  constructor(
    public router: Router,
    private employeeService: EmployeeService,
    public appSettingsService: AppSettingsService,
    ) { }

  ngOnInit(): void {
    this.actions = [
      { key: 'view', label: 'Edit', icon: 'fa fa-eye text-primary', tooltipText: 'Edit' },
      { key: 'delete', label: 'Delete', icon: 'fa fa-trash-alt text-danger', tooltipText: 'Delete' },
    ]
  }

  onLoadItemsEvent(queryParams: QueryParamsModel) {
    console.log('queryParams', queryParams);

    this.employeeService.list(queryParams).subscribe((result) => {
      this.idaTable.loadItems({ items: result.items, totalCount: result.totalCount, errorMessage: '' });
    });
  }

  add(ev) {
    console.log(ev);
    this.router.navigate(['employee/add']);
  }
  /**
   * onActionEvents
   */
  public onActionEvents(event) {
    console.log('onActionEvents', event);
    let key = event.action.key;
    switch (key) {
      case 'view':
        this.router.navigate(['employee/edit/' + event.row.id]);
        break;
      case 'delete':
        const flag = confirm('are you sure want to delete ?');
        if (flag) {
          this.employeeService.delete(event.row.id).subscribe((resp) => {
            if (resp.status) {
              this.appSettingsService.showSuccess('User deleted successfully.');
              this.idaTable.fetchItems();
            }
          });
        }

        break;
      default:
        break;
    }
  }

  edit(data = null) {
  }

}



