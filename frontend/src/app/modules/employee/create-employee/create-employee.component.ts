import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { Router, Params, ActivatedRoute} from '@angular/router';
import { forkJoin } from 'rxjs';
import { AppSettingsService } from 'src/app/core/services/app-settings.service';
import { DepartmentService } from '../../department/department.service';
import { OrganizationService } from '../../organization/organization.service';
import { EmployeeService } from '../employee.service';
@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {
  public id: any;
  public data: any;
  public form: FormGroup;
  public submitted: boolean;
  public roles = [];
  public orgs = [];
  public depts = [];
  public depts_cache = [];
  constructor(
    private _formBuilder: FormBuilder,
    public departmentService: DepartmentService,
    private organizationService: OrganizationService,
    private employeeService: EmployeeService,
    public appSettingsService: AppSettingsService,
    private router: Router,
    private activatedRoute: ActivatedRoute
    ) {}

  ngOnInit(): void {

    this.initForm();

    forkJoin([this.organizationService.list({}), this.departmentService.list({})]).subscribe((resp: any) => {
      if (resp[0].items) {
        this.orgs = resp[0].items || [];
        console.log('orgs', this.orgs);
      }

      if (resp[1].items) {
        this.depts_cache = resp[1].items || [];
        if (this.depts_cache.length) {
          this.updateDepts(this.depts_cache[0].id);
        }

        console.log('orgs', this.orgs);
      }

      this.id = this.activatedRoute.snapshot.params.id;
      if (this.id) {
        this.employeeService.getById(this.id).subscribe((resp) => {
          this.data = resp.data;
          let formValue = {
            organizationId: this.data.organizationId,
            departmentId: this.data.departmentId,
            firstName: this.data.firstName,
            lastName: this.data.lastName,
            dob: this.data.dob,
            workTitle: this.data.workTitle,
            experience: this.data.experience
          };
          this.form.patchValue(formValue);
        });
      }

    });
  }

  get formControls() { return this.form.controls; }
  get organizationId(){return this.form.get('organizationId')}
  get departmentId(){return this.form.get('departmentId')}
  get firstName(){return this.form.get('firstName')}
  get lastName(){return this.form.get('lastName')}
  get dob() {return this.form.get('dob')}
  get workTitle(){return this.form.get('workTitle')}
  get experience(){return this.form.get('experience')}

  initForm() {
    this.form = this._formBuilder.group({
      organizationId: ['', Validators.compose([Validators.required])],
      departmentId: ['', Validators.compose([Validators.required])],
      firstName: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(100)
      ])],
      lastName: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(100)
      ])],
      dob: ['', Validators.compose([Validators.maxLength(100)])],
      workTitle: ['', Validators.compose([Validators.maxLength(100)])],
      experience: ['', Validators.compose([Validators.required])]
    });
  }

  updateDepts(selectedOrg) {
    this.depts = [];
    for (const item of this.depts_cache) {
      if (this.form.value.organizationId == item.organizationId || selectedOrg == item.organizationId) {
        this.depts.push(item);
      }
    }
  }

  add(){
    console.log(this.form.value);
    let form = this.form.value;

    let params = {
      organizationId: form.organizationId,
      departmentId: form.departmentId,
      firstName: form.firstName,
      lastName: form.lastName,
      workTitle: form.workTitle,
      experience: form.experience
    };
    console.log(this.id);

    if (this.id) {
      this.employeeService.update(this.id, params).subscribe((resp) => {
        if (resp.status) {
          this.appSettingsService.showSuccess('Employee updated successfully.');
          this.router.navigate(['employee/list']);
        } else {
          this.appSettingsService.showError(resp.message);
        }
      });
    } else {
      this.employeeService.add(params).subscribe((resp) => {
        if (resp.status) {
          this.appSettingsService.showSuccess('Employee created successfully.');
          this.router.navigate(['employee/list']);
        } else {
          this.appSettingsService.showError(resp.message);
        }
      });
    }
  }
}




