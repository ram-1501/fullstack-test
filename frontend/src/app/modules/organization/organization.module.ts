import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrganizationRoutingModule } from './organization-routing.module';
import { CreateEmployeeComponent } from './create-employee/create-employee.component';
import { CreateOrganizationComponent } from './create-organization/create-organization.component';
import { OrganizationListComponent } from './organization-list/organization-list.component';
import { IdaTableModule } from '../ida-table/ida-table.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    CreateEmployeeComponent,
    CreateOrganizationComponent,
    OrganizationListComponent
  ],
  imports: [
    CommonModule,
    OrganizationRoutingModule,
    IdaTableModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class OrganizationModule { }
