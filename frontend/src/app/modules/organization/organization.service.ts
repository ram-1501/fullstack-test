import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { AppSettingsService } from 'src/app/core/services/app-settings.service';

@Injectable({
  providedIn: 'root'
})
export class OrganizationService {

  constructor(private http:HttpClient) {

  }

  count(){
    return this.http.get('organization/count').pipe(
      map((resp: any) => {
        let count = 0;
        if (resp && resp.data) {
          count = resp.data;
        }
        return count;
      })
    );
  }

  list(params) {
    return this.http.post('organization/list', params).pipe(
      map((resp: any) => {
        let result = {
          items: resp.data.rows,
          totalCount: resp.data.count
        }
        return result;
      })
    );
  }

  add(params){
    return this.http.put('organization', params).pipe(
      map((resp: any) => {
        return resp;
      })
    );
  }

  update(id, params){
    return this.http.patch(`organization/${id}`, params).pipe(
      map((resp: any) => {
        return resp;
      })
    );
  }

  getById(id){
    return this.http.get(`organization/${id}`).pipe(
      map((resp: any) => {
        return resp;
      })
    );
  }

  delete(id){
    return this.http.delete(`organization/${id}`).pipe(
      map((resp: any) => {
        return resp;
      })
    );
  }
}
