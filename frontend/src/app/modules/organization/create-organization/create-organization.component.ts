import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { Router, Params, ActivatedRoute} from '@angular/router';
import { AppSettingsService } from 'src/app/core/services/app-settings.service';
import { OrganizationService } from '../organization.service';
@Component({
  selector: 'app-create-organization',
  templateUrl: './create-organization.component.html',
  styleUrls: ['./create-organization.component.css']
})
export class CreateOrganizationComponent implements OnInit {
  public id: any;
  public data: any;
  public form: FormGroup;
  public submitted: boolean;
  public roles = [];
  constructor(
    private _formBuilder: FormBuilder,
    public organizationService: OrganizationService,
    public appSettingsService: AppSettingsService,
    private router: Router,
    private activatedRoute: ActivatedRoute
    ) {}

  ngOnInit(): void {

    this.initForm();

    this.id = this.activatedRoute.snapshot.params.id;
    if (this.id) {
      this.organizationService.getById(this.id).subscribe((resp) => {
        this.data = resp.data;
        let formValue = {
          name: this.data.name,
          owner: this.data.owner,
          address: this.data.address,
          city: this.data.city,
          state: this.data.state,
          country: this.data.country
        };
        this.form.patchValue(formValue);
      });
    }

  }

  get formControls() { return this.form.controls; }
  get name(){return this.form.get('name')}
  get owner(){return this.form.get('owner')}
  get address(){return this.form.get('address')}
  get city(){return this.form.get('city')}
  get state(){return this.form.get('state')}
  get country(){return this.form.get('country')}

  initForm() {
    this.form = this._formBuilder.group({
      name: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(100),
        Validators.minLength(4)
      ])],
      owner: ['', Validators.compose([Validators.maxLength(100)])],
      address: ['', Validators.compose([Validators.required,Validators.email])],
      city: ['', Validators.compose([Validators.required,Validators.minLength(6)])],
      state: ['', Validators.compose([Validators.maxLength(100)])],
      country: ['', Validators.compose([Validators.minLength(10)])]
    });
  }

  add(){
    console.log(this.form.value);
    let form = this.form.value;

    let params = {
      name: form.name,
      owner: form.owner,
      address: form.address,
      city: form.city,
      state: form.state,
      country:form.country
    };
    console.log(this.id);

    if (this.id) {
      this.organizationService.update(this.id, params).subscribe((resp) => {
        if (resp.status) {
          this.appSettingsService.showSuccess('Organization updated successfully.');
          this.router.navigate(['organization/list']);
        } else {
          this.appSettingsService.showError(resp.message);
        }
      });
    } else {
      this.organizationService.add(params).subscribe((resp) => {
        if (resp.status) {
          this.appSettingsService.showSuccess('Organization created successfully.');
          this.router.navigate(['organization/list']);
        } else {
          this.appSettingsService.showError(resp.message);
        }
      });
    }

  }
}


