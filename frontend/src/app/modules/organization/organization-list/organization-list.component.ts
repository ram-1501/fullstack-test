import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AppSettingsService } from 'src/app/core/services/app-settings.service';
import { DataTableActionInterface } from 'src/app/modules/ida-table/ida-table.model';
import { IdaTableComponent } from 'src/app/modules/ida-table/ida-table/ida-table.component';
import { QueryParamsModel } from 'src/app/modules/ida-table/query-params.model';
import { OrganizationService } from '../organization.service';

@Component({
  selector: 'app-organization-list',
  templateUrl: './organization-list.component.html',
  styleUrls: ['./organization-list.component.css']
})
export class OrganizationListComponent implements OnInit {
  @ViewChild('idaTable', { static: true }) idaTable: IdaTableComponent;

  public displayColumns = [
    { prop: 'name', name: 'name', sortable: true, search: true },
    { prop: 'owner', name: 'Owner', sortable: true, search: true },
    { prop: 'address', name: 'Address', sortable: true, search: true },
    { prop: 'city', name: 'City', sortable: true, search: true },
    { prop: 'state', name: 'State', sortable: true, search: true },
    { prop: 'country', name: 'Country', sortable: true, search: true },
    { prop: 'action', name: 'Action', sortable: true, show: true, cellTemplate: 'action' }
  ];
  actions: Array<DataTableActionInterface> = [];

  constructor(
    public router: Router,
    private organizationService: OrganizationService,
    public appSettingsService: AppSettingsService,
    ) { }

  ngOnInit(): void {
    this.actions = [
      { key: 'view', label: 'Edit', icon: 'fa fa-eye text-primary', tooltipText: 'Edit' },
      { key: 'delete', label: 'Delete', icon: 'fa fa-trash-alt text-danger', tooltipText: 'Delete' },
    ]
  }

  onLoadItemsEvent(queryParams: QueryParamsModel) {
    console.log('queryParams', queryParams);

    this.organizationService.list(queryParams).subscribe((result) => {
      this.idaTable.loadItems({ items: result.items, totalCount: result.totalCount, errorMessage: '' });
    });
  }

  add(ev) {
    console.log(ev);
    this.router.navigate(['organization/add']);
  }
  /**
   * onActionEvents
   */
  public onActionEvents(event) {
    console.log('onActionEvents', event);
    let key = event.action.key;
    switch (key) {
      case 'view':
        this.router.navigate(['organization/edit/' + event.row.id]);
        break;
      case 'delete':
        const flag = confirm('are you sure want to delete ?');
        if (flag) {
          this.organizationService.delete(event.row.id).subscribe((resp) => {
            if (resp.status) {
              this.appSettingsService.showSuccess('User deleted successfully.');
              this.idaTable.fetchItems();
            }
          });
        }

        break;
      default:
        break;
    }
  }

  edit(data = null) {
  }

}


