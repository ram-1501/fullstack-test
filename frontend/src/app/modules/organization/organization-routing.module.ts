import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateOrganizationComponent } from './create-organization/create-organization.component';
import { OrganizationListComponent } from './organization-list/organization-list.component';

const routes: Routes = [
  {
    path: '',
    component: OrganizationListComponent
  },
  {
    path: 'list',
    component: OrganizationListComponent
  },
  {
    path: 'add',
    component: CreateOrganizationComponent
  }
  ,
  {
    path: 'edit/:id',
    component: CreateOrganizationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganizationRoutingModule { }
