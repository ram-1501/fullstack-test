import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { of } from 'rxjs';
import { AppSettingsService } from 'src/app/core/services/app-settings.service';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient,
    private appSettingsService: AppSettingsService) {

  }

  list(params) {
    return this.http.post('user/list', params).pipe(
      map((resp: any) => {
        let result = {
          items: resp.data.rows,
          totalCount: resp.data.count
        }
        return result;
      })
    );
  }

  addUser(params){
    return this.http.put('user', params).pipe(
      map((resp: any) => {
        return resp;
      })
    );
  }

  updateUser(userId, params){
    return this.http.patch(`user/${userId}`, params).pipe(
      map((resp: any) => {
        return resp;
      })
    );
  }

  getUserById(userId){
    return this.http.get(`user/${userId}`).pipe(
      map((resp: any) => {
        return resp;
      })
    );
  }

  deleteUser(userId){
    return this.http.delete(`user/${userId}`).pipe(
      map((resp: any) => {
        return resp;
      })
    );
  }
}
