import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateUserComponent } from './pages/create-user/create-user.component';
import { UserListComponent } from './pages/user-list/user-list.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full'
  },
  {
    path: 'list',
    component: UserListComponent,
    data: {
      title: 'User List',
      urls: [
        { title: 'Dashboard', url: '/dashboard' },
        { title: 'User List' }
      ]
    }
  },
  {
    path: 'add',
    component: CreateUserComponent,
    data: {
      title: 'Create User',
      urls: [
        { title: 'Dashboard', url: '/dashboard' },
        { title: 'User List', url: '/users/list' },
        { title: 'Create User' }
      ]
    }
  },
  {
    path: 'edit/:id',
    component: CreateUserComponent,
    data: {
      title: 'Edit User',
      urls: [
        { title: 'Dashboard', url: '/dashboard' },
        { title: 'User List', url: '/users/list' },
        { title: 'Edit User' }
      ]
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
