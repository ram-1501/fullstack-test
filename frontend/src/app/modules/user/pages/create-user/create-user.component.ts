import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { Router, Params, ActivatedRoute} from '@angular/router';
import { AppSettingsService } from 'src/app/core/services/app-settings.service';
import { UserService } from '../../services/user.service'
@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  public userId: any;
  public user: any;
  public userForm: FormGroup;
  public submitted: boolean;
  public roles = [];
  public defaultPassword = '******';
  constructor(
    private _formBuilder: FormBuilder,
    public userService: UserService,
    public appSettingsService: AppSettingsService,
    private router: Router,
    private activatedRoute: ActivatedRoute
    ) {}

  ngOnInit(): void {

    this.appSettingsService.getRoles().subscribe((roles) => {
      this.roles = roles;
      this.initForm();

      this.userId = this.activatedRoute.snapshot.params.id;
      this.userService.getUserById(this.userId).subscribe((resp) => {
        this.user = resp.data;
        let formValue = {
          email: this.user.email,
          firstName: this.user.firstName,
          lastName: this.user.lastName,
          password: this.defaultPassword,
          company: this.user.company,
          phone: this.user.phone,
          roleId: '',
          active: ''+this.user.active,
        };

        if (Array.isArray(this.user.userRoles) && this.user.userRoles.length) {
          formValue.roleId = this.user.userRoles[0].roleId;
        };

        console.log('formValue', formValue);

        this.userForm.patchValue(formValue);
        this.userForm.get('password').setValidators([]);
        this.userForm.updateValueAndValidity();
      });

    });



  }

  get formControls() { return this.userForm.controls; }
  get firstName(){return this.userForm.get('firstName')}
  get lastName(){return this.userForm.get('lastName')}
  get email(){return this.userForm.get('email')}
  get password(){return this.userForm.get('password')}
  get company(){return this.userForm.get('company')}
  get phone(){return this.userForm.get('phone')}
  get roleId(){return this.userForm.get('roleId')}
  get active(){return this.userForm.get('active')}

  initForm() {
    this.userForm = this._formBuilder.group({
      firstName: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(100),
        Validators.minLength(4)
      ])],
      lastName: ['', Validators.compose([Validators.maxLength(100)])],
      email: ['', Validators.compose([Validators.required,Validators.email])],
      password: ['', Validators.compose([Validators.required,Validators.minLength(6)])],
      company: ['', Validators.compose([Validators.maxLength(100)])],
      phone: ['', Validators.compose([Validators.minLength(10)])],
      roleId: ['2', Validators.compose([Validators.required])],
      active: ['1', Validators.compose([Validators.required])],
    });
  }

  adduser(){
    console.log(this.userForm.value);
    let form = this.userForm.value;

    let params = {
      userName: form.email,
      firstName: form.firstName,
      lastName: form.lastName,
      email: form.email,
      password: form.password,
      company:form.company,
      picture: '',
      phone: form.phone,
      active: parseInt(form.active),
      roleId: parseInt(form.roleId),
    };

    if (this.userId) {
      this.userService.updateUser(this.userId, params).subscribe((resp) => {
        if (resp.status) {
          this.appSettingsService.showSuccess('User updated successfully.');
          this.router.navigate(['users/list']);
        } else {
          this.appSettingsService.showError(resp.message);
        }
      });
    } else {
      this.userService.addUser(params).subscribe((resp) => {
        if (resp.status) {
          this.appSettingsService.showSuccess('User created successfully.');
          this.router.navigate(['users/list']);
        } else {
          this.appSettingsService.showError(resp.message);
        }
      });
    }

  }
}
