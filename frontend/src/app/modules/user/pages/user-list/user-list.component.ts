import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AppSettingsService } from 'src/app/core/services/app-settings.service';
import { DataTableActionInterface } from 'src/app/modules/ida-table/ida-table.model';
import { IdaTableComponent } from 'src/app/modules/ida-table/ida-table/ida-table.component';
import { QueryParamsModel } from 'src/app/modules/ida-table/query-params.model';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  @ViewChild('idaTable', { static: true }) idaTable: IdaTableComponent;

  public displayColumns = [
    { prop: 'userName', name: 'Username', sortable: true, search: true },
    { prop: 'firstName', name: 'FirstName', sortable: true, search: true },
    { prop: 'lastName', name: 'LastName', sortable: true, search: true },
    { prop: 'email', name: 'Email', sortable: true, search: true },
    { prop: 'phone', name: 'Phone', sortable: true, search: true },
    { prop: 'action', name: 'Action', sortable: true, show: true, cellTemplate: 'action' }
  ];
  actions: Array<DataTableActionInterface> = [];

  constructor(
    public router: Router,
    private userService: UserService,
    public appSettingsService: AppSettingsService,
    ) { }

  ngOnInit(): void {
    this.actions = [
      { key: 'view', label: 'View', icon: 'fa fa-eye text-primary', tooltipText: 'View' },
      { key: 'edit', label: 'Edit', icon: 'fa fa-edit text-success', tooltipText: 'Edit' },
      { key: 'delete', label: 'Delete', icon: 'fa fa-trash-alt text-danger', tooltipText: 'Delete' },
    ]
  }

  onLoadItemsEvent(queryParams: QueryParamsModel) {
    console.log('queryParams', queryParams);

    this.userService.list(queryParams).subscribe((result) => {
      this.idaTable.loadItems({ items: result.items, totalCount: result.totalCount, errorMessage: '' });
    });
  }

  addUser(ev) {
    console.log(ev);
    this.router.navigate(['users/add']);
  }
  /**
   * onActionEvents
   */
  public onActionEvents(event) {
    console.log('onActionEvents', event);
    let key = event.action.key;
    switch (key) {
      case 'edit':
        this.router.navigate(['users/edit/' + event.row.id]);
        break;
      case 'view':
        this.router.navigate(['users/view/' + event.row.id]);
        break;
      case 'delete':
        const flag = confirm('are you sure want to delete ?');
        if (flag) {
          this.userService.deleteUser(event.row.id).subscribe((resp) => {
            if (resp.status) {
              this.appSettingsService.showSuccess('User deleted successfully.');
              this.idaTable.fetchItems();
            }
          });
        }

        break;
      default:
        break;
    }
  }

  edit(data = null) {
  }

}

