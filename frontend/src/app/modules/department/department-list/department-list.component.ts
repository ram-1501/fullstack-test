import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AppSettingsService } from 'src/app/core/services/app-settings.service';
import { DataTableActionInterface } from 'src/app/modules/ida-table/ida-table.model';
import { IdaTableComponent } from 'src/app/modules/ida-table/ida-table/ida-table.component';
import { QueryParamsModel } from 'src/app/modules/ida-table/query-params.model';
import { DepartmentService } from '../department.service';

@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
  styleUrls: ['./department-list.component.css']
})
export class DepartmentListComponent implements OnInit {
  @ViewChild('idaTable', { static: true }) idaTable: IdaTableComponent;

  public displayColumns = [
    { prop: 'orgName', name: 'Organization', sortable: true, search: true },
    { prop: 'name', name: 'name', sortable: true, search: true },
    { prop: 'owner', name: 'Owner', sortable: true, search: true },
    { prop: 'description', name: 'Description', sortable: true, search: true },
    { prop: 'workingTime', name: 'Working Time', sortable: true, search: true },
    { prop: 'workingDays', name: 'Working Days', sortable: true, search: true },
    { prop: 'action', name: 'Action', sortable: true, show: true, cellTemplate: 'action' }
  ];
  actions: Array<DataTableActionInterface> = [];

  constructor(
    public router: Router,
    private departmentService: DepartmentService,
    public appSettingsService: AppSettingsService,
    ) { }

  ngOnInit(): void {
    this.actions = [
      { key: 'view', label: 'Edit', icon: 'fa fa-eye text-primary', tooltipText: 'Edit' },
      { key: 'delete', label: 'Delete', icon: 'fa fa-trash-alt text-danger', tooltipText: 'Delete' },
    ]
  }

  onLoadItemsEvent(queryParams: QueryParamsModel) {
    console.log('queryParams', queryParams);

    this.departmentService.list(queryParams).subscribe((result) => {
      this.idaTable.loadItems({ items: result.items, totalCount: result.totalCount, errorMessage: '' });
    });
  }

  add(ev) {
    console.log(ev);
    this.router.navigate(['department/add']);
  }
  /**
   * onActionEvents
   */
  public onActionEvents(event) {
    console.log('onActionEvents', event);
    let key = event.action.key;
    switch (key) {
      case 'view':
        this.router.navigate(['department/edit/' + event.row.id]);
        break;
      case 'delete':
        const flag = confirm('are you sure want to delete ?');
        if (flag) {
          this.departmentService.delete(event.row.id).subscribe((resp) => {
            if (resp.status) {
              this.appSettingsService.showSuccess('User deleted successfully.');
              this.idaTable.fetchItems();
            }
          });
        }

        break;
      default:
        break;
    }
  }

  edit(data = null) {
  }

}



