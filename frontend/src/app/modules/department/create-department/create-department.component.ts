import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { Router, Params, ActivatedRoute} from '@angular/router';
import { AppSettingsService } from 'src/app/core/services/app-settings.service';
import { OrganizationService } from '../../organization/organization.service';
import { DepartmentService } from '../department.service';
@Component({
  selector: 'app-create-department',
  templateUrl: './create-department.component.html',
  styleUrls: ['./create-department.component.css']
})
export class CreateDepartmentComponent implements OnInit {
  public id: any;
  public data: any;
  public form: FormGroup;
  public submitted: boolean;
  public roles = [];
  public orgs = [];
  constructor(
    private _formBuilder: FormBuilder,
    public departmentService: DepartmentService,
    private organizationService: OrganizationService,
    public appSettingsService: AppSettingsService,
    private router: Router,
    private activatedRoute: ActivatedRoute
    ) {}

  ngOnInit(): void {

    this.initForm();

    this.organizationService.list({}).subscribe((resp: any) => {
      if (resp.items) {
        this.orgs = resp.items || [];
        console.log('orgs', this.orgs);
      }

      this.id = this.activatedRoute.snapshot.params.id;
      if (this.id) {
        this.departmentService.getById(this.id).subscribe((resp) => {
          this.data = resp.data;
          let formValue = {
            organizationId: this.data.organizationId,
            name: this.data.name,
            owner: this.data.owner,
            description: this.data.description,
            workingTime: this.data.workingTime,
            workingDays: this.data.workingDays
          };
          this.form.patchValue(formValue);
        });
      }

    });
  }

  get formControls() { return this.form.controls; }
  get organizationId(){return this.form.get('organizationId')}
  get name(){return this.form.get('name')}
  get owner(){return this.form.get('owner')}
  get description(){return this.form.get('description')}
  get workingTime(){return this.form.get('workingTime')}
  get workingDays(){return this.form.get('workingDays')}

  initForm() {
    this.form = this._formBuilder.group({
      organizationId: ['', Validators.compose([Validators.required])],
      name: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(100)
      ])],
      owner: ['', Validators.compose([Validators.maxLength(100)])],
      description: ['', Validators.compose([Validators.required])],
      workingTime: ['', Validators.compose([Validators.required])],
      workingDays: ['', Validators.compose([Validators.maxLength(100)])]
    });
  }

  add(){
    console.log(this.form.value);
    let form = this.form.value;

    let params = {
      organizationId: form.organizationId,
      name: form.name,
      owner: form.owner,
      description: form.description,
      workingTime: form.workingTime,
      workingDays: form.workingDays
    };
    console.log(this.id);

    if (this.id) {
      this.departmentService.update(this.id, params).subscribe((resp) => {
        if (resp.status) {
          this.appSettingsService.showSuccess('Department updated successfully.');
          this.router.navigate(['department/list']);
        } else {
          this.appSettingsService.showError(resp.message);
        }
      });
    } else {
      this.departmentService.add(params).subscribe((resp) => {
        if (resp.status) {
          this.appSettingsService.showSuccess('Department created successfully.');
          this.router.navigate(['department/list']);
        } else {
          this.appSettingsService.showError(resp.message);
        }
      });
    }
  }
}



