import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { AppSettingsService } from 'src/app/core/services/app-settings.service';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {

  constructor(private http:HttpClient) {

  }

  count(){
    return this.http.get('department/count').pipe(
      map((resp: any) => {
        let count = 0;
        if (resp && resp.data) {
          count = resp.data;
        }
        return count;
      })
    );
  }

  list(params) {
    return this.http.post('department/list', params).pipe(
      map((resp: any) => {
        let result = {
          items: [],
          totalCount: resp.data.count
        };

        for (const item of resp.data.rows) {
          item.orgName = item.organization ? item.organization.name : '',
          result.items.push(item);
        }

        return result;
      })
    );
  }

  add(params){
    return this.http.put('department', params).pipe(
      map((resp: any) => {
        return resp;
      })
    );
  }

  update(id, params){
    return this.http.patch(`department/${id}`, params).pipe(
      map((resp: any) => {
        return resp;
      })
    );
  }

  getById(id){
    return this.http.get(`department/${id}`).pipe(
      map((resp: any) => {
        return resp;
      })
    );
  }

  delete(id){
    return this.http.delete(`department/${id}`).pipe(
      map((resp: any) => {
        return resp;
      })
    );
  }
}
