import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateDepartmentComponent } from './create-department/create-department.component';
import { DepartmentListComponent } from './department-list/department-list.component';

const routes: Routes = [
  {
    path: '',
    component: DepartmentListComponent
  },
  {
    path: 'list',
    component: DepartmentListComponent
  },
  {
    path: 'add',
    component: CreateDepartmentComponent
  }
  ,
  {
    path: 'edit/:id',
    component: CreateDepartmentComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DepartmentRoutingModule { }
