import { Injectable } from '@angular/core';
import { map, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { AppSettingsService } from 'src/app/core/services/app-settings.service';
// import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http:HttpClient,
    private appSettingsService: AppSettingsService
  ) { }


  /**
   * login
   */
   public login(params: any) {
    let httpHeaders = { showLoader: 'true' };

    return this.http.post('auth/login', params, { headers: httpHeaders }).pipe(
      mergeMap((resp: any) => {
        return this.appSettingsService.setUserToken(resp.data.token).pipe(
          map(() => {
            return resp;
          })
        );
      })
    );
  }

  /**
   * signup
   */
   public signup(params: any) {
    let httpHeaders = { showLoader: 'true' };

    return this.http.post('auth/signup', params, { headers: httpHeaders }).pipe(
      map((resp: any) => {
        return resp;
      })
    );
  }

}
