import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppSettingsService } from 'src/app/core/services/app-settings.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  recoverform = false;

  loginForm = new FormGroup({
    email : new FormControl('',[Validators.required,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]),
    password: new FormControl('',[Validators.required])
  })

  errorMessage = '';
  constructor(
    private router: Router,
    private authService:AuthService,
    private appSettingsService:AppSettingsService
  ) { }

  get email() {return this.loginForm.get('email')};
  get password() {return this.loginForm.get('password')};

  ngOnInit(): void {
  }


  showRecoverForm() {
    this.recoverform = !this.recoverform;
  }

  /**
   * doLogin
   */
   public doLogin() {
    let params = this.loginForm.value;
    console.log(params);
    /*let params = {
      email:"admin@admin.com",
      password:"admin"
    };*/

    this.appSettingsService.showLoader();
    this.authService.login(params).subscribe((resp) => {
      this.appSettingsService.hideLoader();
      console.log('doLogin', resp);
      if (!resp.status) {
        this.errorMessage = resp.message;
        setTimeout(() => {
          this.errorMessage = '';
        }, 1000*3);
      } else {
        this.router.navigate(['dashboard'], { queryParams: {} });
      }
    });
  }

}
