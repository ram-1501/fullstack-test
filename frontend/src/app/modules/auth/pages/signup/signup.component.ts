import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppSettingsService } from 'src/app/core/services/app-settings.service';
import { AuthService } from '../../services/auth.service';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html'
})
export class SignupComponent implements OnInit {
  form: FormGroup;


  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService:AuthService,
    private appSettingsService: AppSettingsService
    ) {

  }

  ngOnInit(): void {
    this.form = this.fb.group({
      firstName: ['', [Validators.required]],
      lastName: ['', []],
      email: ['', [Validators.required,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirm_password: ['', [Validators.required, Validators.minLength(6)]]
    }, {
      validator: ConfirmedValidator('password', 'confirm_password')
    })
  }

  get firstName() {return this.form.get('firstName')};
  get lastName() {return this.form.get('lastName')};
  get email() {return this.form.get('email')};
  get password() {return this.form.get('password')};
  get confirm_password() {return this.form.get('confirm_password')};

  signup() {
    console.log(this.form, this.form.value);

    let params = this.form.value;
    params.roleId = 2;
    this.authService.signup(params).subscribe((resp) => {
      if (resp.status) {
        this.appSettingsService.showSuccess('User created successfully.');
        this.router.navigate(['auth/login']);
      } else {
        this.appSettingsService.showError(resp.message);
      }
    });
  }

}


export function ConfirmedValidator(controlName: string, matchingControlName: string){
  return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];
      if (matchingControl.errors && !matchingControl.errors.confirmedValidator) {
          return;
      }
      if (control.value !== matchingControl.value) {
          matchingControl.setErrors({ confirmedValidator: true });
      } else {
          matchingControl.setErrors(null);
      }
  }
}
