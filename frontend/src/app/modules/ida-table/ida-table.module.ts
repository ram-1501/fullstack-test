import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IdaTableComponent } from './ida-table/ida-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgbModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { FeatherModule } from 'angular-feather';



@NgModule({
  declarations: [IdaTableComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    NgbModule,
    NgbTooltipModule,
    FeatherModule
  ],
  exports: [IdaTableComponent]
})
export class IdaTableModule { }
