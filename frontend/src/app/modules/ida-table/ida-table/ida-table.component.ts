import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ColumnMode, SelectionType } from '@swimlane/ngx-datatable';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { AppSettingsService } from 'src/app/core/services/app-settings.service';
import { DataTableActionInterface, DataTableBulkActionInterface } from '../ida-table.model';
import { QueryParamsModel } from '../query-params.model';
import { QueryResultsModel } from '../query-result.model';

@Component({
  selector: 'app-ida-table',
  templateUrl: './ida-table.component.html',
  styleUrls: ['./ida-table.component.css']
})
export class IdaTableComponent implements OnInit {

  @Input() tableType: string = 'default';
  @Input() title: string = '';
  @Input() displayColumns: Array<any> = [];
  @Input() showCheckboxColumn: boolean = false;
  @Input() perPageOptions: Array<any> = [10, 20, 100, 200, 500, 1000];
  @Input() actions: Array<DataTableActionInterface> = [];
  @Input() bulkActions: Array<DataTableBulkActionInterface> = [];
  @Input() addText: string = 'Add';

  @Output() onActionEvents = new EventEmitter();
  @Output() onBulkActionEvents = new EventEmitter();
  @Output() onLoadItemsEvent = new EventEmitter();
  @Output() onAddClick = new EventEmitter();

  @ViewChild('priceTemplate', { static: true }) priceTemplate: TemplateRef<any>;
  @ViewChild('actionTemplate', { static: true }) actionTemplate: TemplateRef<any>;
  @ViewChild('productImageTemplate', { static: true }) productImageTemplate: TemplateRef<any>;
  @ViewChild('productLinkTemplate', { static: true }) productLinkTemplate: TemplateRef<any>;
  @ViewChild('statusTemplate', { static: true }) statusTemplate: TemplateRef<any>;
  @ViewChild('productStatusTemplate', { static: true }) productStatusTemplate: TemplateRef<any>;

  public rows = [];
  public columns = [];

  public sort: { dir: string, prop: string } = {
    dir: 'asc',
    prop: 'id'
  };

  public paginator = {
    pageIndex: 0,
    pageNumber: 1,
    pageSize: 10,
    totalCount: 50
  };

  public perPage = 10;
  public searchInput = '';
  public showFilter = true;
  public simpleSearchKeys: Array<any> = [];
  public selectedSimpleSearchKey;

  joke = 'knock knock';
  ColumnMode = ColumnMode;
  SelectionType = SelectionType;
  selectedRows = [];

  constructor(private appSettingsService: AppSettingsService, private router: Router) { }

  ngOnInit(): void {
    this.initColumns();
    this.prepareColumns();
    this.setPageNumber(1);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.initColumns();
    this.prepareColumns();
  }

  initColumns() {
    this.simpleSearchKeys = [{ prop: 'all', name: 'All' }];
    for (const displayColumn of this.displayColumns) {
      displayColumn.show = (displayColumn.show === false) ? false : true;
      if (displayColumn.cellTemplate) {
        switch (displayColumn.cellTemplate) {
          case 'price':
            // let context = {pp: temp.prp};
            // this.priceTemplate.createEmbeddedView({$implicit: {description: 'some description'} });
            displayColumn.cellTemplate = this.priceTemplate;

            break;
          case 'productImage':
            displayColumn.cellTemplate = this.productImageTemplate;
            break;

          case 'productLink':
            displayColumn.cellTemplate = this.productLinkTemplate;
            break;

          case 'action':
            displayColumn.cellTemplate = this.actionTemplate;
            break;

          case 'status':
            displayColumn.cellTemplate = this.statusTemplate;
            break;
          case 'productStatus':
            displayColumn.cellTemplate = this.statusTemplate;
            break;
          default:
            break;
        }
      }

      if (displayColumn.search) {
        this.simpleSearchKeys.push({ ...displayColumn });
      }
    }

    this.selectedSimpleSearchKey = this.simpleSearchKeys[0];
  }

  prepareColumns() {
    this.columns = [];

    if (this.showCheckboxColumn) {
      this.columns.push({
        prop: 'selected',
        name: '',
        soratble: false,
        draggable: false,
        checkboxable: true,
        headerCheckboxable: true,
        width: 30,
        show: false
      });
    }

    for (const displayColumn of this.displayColumns) {
      if (displayColumn.show) {
        this.columns.push(displayColumn)
      }
    }
  }

  onSort($event) {
    this.sort = $event.sorts.pop();
    console.log($event);
    this.fetchItems();
  }

  setPageNumber($event) {
    this.paginator.pageIndex = $event - 1;
    this.paginator.pageNumber = $event;
    this.fetchItems();
    console.log('setPageNumber', this.paginator);
  }

  setPerPageOption() {
    this.paginator.pageSize = parseInt(''+this.perPage);
    this.paginator.pageIndex = 0;
    this.paginator.pageNumber = 1;
    this.fetchItems();
    console.log('setPerPageOption', this.paginator);
  }

  doSimpleSearch(data) {
    // if (data.prop == 'all') {
    //   this.selectedSimpleSearchKey  = null;
    // } else {
    //   this.selectedSimpleSearchKey  = data;
    // }
    this.selectedSimpleSearchKey = data;
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => {
        // if (term.length > 2 || term.length === 0) {
        this.fetchItems();
        // }
      })
    )


  loadItems(queryResults: QueryResultsModel) {
    this.rows = queryResults.items;
    this.paginator = { ...this.paginator, ...{ totalCount: queryResults.totalCount } };
  }

  /**
   * fetchItems
   *
   */
  fetchItems() {

    const queryParams = new QueryParamsModel(
      this.filterConfiguration(),
      this.sort.dir,
      this.sort.prop,
      this.paginator.pageIndex,
      this.paginator.pageSize,
      (this.searchInput) ? this.searchInput : ""
    );

    this.onLoadItemsEvent.emit(queryParams);
  }

  filterConfiguration() {
    let filter: any = {
      serachableFields: []
    };

    if (this.selectedSimpleSearchKey) {
      if (this.selectedSimpleSearchKey.prop == 'all') {
        for (const simpleSearchKey of this.simpleSearchKeys) {
          if (simpleSearchKey.prop !== 'all') {
            filter.serachableFields.push(simpleSearchKey.prop);
          }
        }
      } else {
        filter.serachableFields = [this.selectedSimpleSearchKey.prop];
      }
    }

    return filter;
  }

  onCreate() {
    this.onAddClick.emit({ key: 'add', label: 'Add' });
  }

  onRowSelect({ selected }) {
    this.selectedRows = selected;
    console.log(this.selectedRows);
  }

  /**
   * onActionClick
   */
  public onActionClick(action, row) {
    console.log('view::view', row, action);
    this.onActionEvents.emit({ action, row });
  }


  updateColumns() {
    console.log('updateColumns');
    this.prepareColumns();
    this.rows = [...this.rows];
  }

  onTreeAction(event: any) {
    const index = event.rowIndex;
    const row = event.row;
    console.log(event, row, row.treeStatus);
    if (row.treeStatus === 'collapsed') {
      row.treeStatus = 'expanded';
    } else {
      row.treeStatus = 'collapsed';
    }
    this.rows = [...this.rows];
  }

  getTextColor(value, row = null) {
    let color = '';
    switch (value) {
      case 'Enabled':
      case 'Active':
      case 'Yes':
        color = 'text-success';
        break;
      case 'Disabled':
      case 'In Active':
      case 'Rejected':
        color = 'text-danger';
        break;
      case 'Pending':
      case 'No':
        color = 'text-grey';
        break;

      default:
        break;
    }

    return color;

  }

  onBulAction(key) {

    if (this.selectedRows.length == 0) {
      // this.appSettingsService.showErrorMessage('Please select atleast one item');
      return;
    }

    switch (key) {
      case 'export':

        let data = [];
        for (let sel of this.selectedRows) {
          let row = {};
          for (let column of this.displayColumns) {

            if (sel[column.prop]) {
              row[column.prop] = sel[column.prop];
            }
          }

          data.push(row);
        }
        console.log(data);

        const options = {
          fieldSeparator: ',',
          quoteStrings: '"',
          decimalSeparator: '.',
          filename: "export",
          showLabels: true,
          title: '',
          useTextFile: false,
          useBom: true,
          useKeysAsHeaders: true,
          // headers: ['Column 1', 'Column 2', etc...] <-- Won't work with useKeysAsHeaders present!
        };

        // const csvExporter = new ExportToCsv(options);

        // csvExporter.generateCsv(data);
        break;
      default:
        break;
    }
  }

  navigateTo(link) {
    this.router.navigate([link]);
  }

  openModal(test,test1){

  }

}
