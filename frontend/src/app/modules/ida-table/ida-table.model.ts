export interface DataTableActionInterface {
  key: string;
  label?: string;
  icon?: string;
  tooltipText?: string;
  options?: Array<DataTableActionActionsInterface>
}

export interface DataTableBulkActionInterface {
  key: string;
  label?: string;
}

export interface DataTableActionActionsInterface {
  key: any;
  label?: string;
}


