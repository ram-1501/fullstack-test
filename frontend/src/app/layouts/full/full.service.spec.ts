import { TestBed } from '@angular/core/testing';

import { FullService } from './full.service';

describe('FullService', () => {
  let service: FullService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FullService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
