import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { AppSettingsService } from 'src/app/core/services/app-settings.service';

@Component({
  selector: 'app-full-layout',
  templateUrl: './full.component.html',
  styleUrls: ['./full.component.scss']
})
export class FullComponent implements OnInit {
  public config: PerfectScrollbarConfigInterface = {};

  constructor(public router: Router, private appSettingsService: AppSettingsService) { }

  tabStatus = 'justified';

  public isCollapsed = false;

  public innerWidth: any;
  public defaultSidebar: any;
  public showSettings = false;
  public showMobileMenu = false;
  public expandLogo = false;



  options = {
    theme: 'light', // two possible values: light, dark
    dir: 'ltr', // two possible values: ltr, rtl
    layout: 'vertical', // two possible values: vertical, horizontal
    sidebartype: 'full', // four possible values: full, iconbar, overlay, mini-sidebar
    sidebarpos: 'fixed', // two possible values: fixed, absolute
    headerpos: 'fixed', // two possible values: fixed, absolute
    boxed: 'full', // two possible values: full, boxed
    navbarbg: 'skin5', // six possible values: skin(1/2/3/4/5/6)
    sidebarbg: 'skin5', // six possible values: skin(1/2/3/4/5/6)
    logobg: 'skin6' // six possible values: skin(1/2/3/4/5/6)
  };

  Logo() {
    this.expandLogo = !this.expandLogo;
  }

  ngOnInit() {

    if (this.router.url === '/') {
      this.router.navigate(['/dashboard/classic']);
    }
    this.defaultSidebar = this.options.sidebartype;
    this.handleSidebar();

    this.appSettingsService.themeSettings$.subscribe((options) => {
      if (options) {
        this.options = {...this.options, ...options};
        console.log('this.options', this.options);

      }
    });

    document.addEventListener('copy', function(e){
      const clipboardData = e.clipboardData || window['clipboardData'];
      let pastedData = e.target ? e.target['innerText']: '';
      clipboardData.setData('text/plain', pastedData);
      e.preventDefault(); // default behaviour is to copy any selected text
    });

    // this.options.layout = localStorage.getItem('layout')==='horizontal'? 'horizontal':'vertical';

    // this.options.boxed = localStorage.getItem('boxed')==='full' ? 'full' : 'boxed';

    // this.options.theme = localStorage.getItem('theme')==='light' ? 'light' : 'dark';

    // this.options.sidebarpos = localStorage.getItem('sidebarpos')==='fixed' ? 'fixed' : 'absolute';

    // this.options.headerpos = localStorage.getItem('headerpos')==='fixed' ? 'fixed' : 'absolute';

    // this.options.dir = localStorage.getItem('dir')==='rtl' ? 'rtl' : 'ltr';

    // this.options.sidebartype = localStorage.getItem('sidebar');

    // this.options.logobg = localStorage.getItem('logobg');

    // this.options.navbarbg = localStorage.getItem('navbar');

    // this.options.sidebarbg = localStorage.getItem('sidebarbg');
  }

  ngAfterViewInit() {
    $(document).on('click', (event) => {
      //to close theme settings option
			if (this.showSettings && !$(event.target).parents('.show-service-panel').length) {
        this.showSettings = false;
			}
		});
  }

  toggleHorizontal(){
    localStorage.setItem('layout',this.options.layout == 'vertical' ? 'horizontal' : 'vertical');
    return (this.options.layout == 'vertical' ? 'horizontal' : 'vertical');
  }

  boxedChange(){
    localStorage.setItem('boxed',this.options.boxed == 'full' ? 'boxed' : 'full');
    return (this.options.boxed == 'full' ? 'boxed' : 'full');
  }

  themeChange(){
    localStorage.setItem('theme',this.options.theme == 'light' ? 'dark' : 'light');
    return (this.options.theme == 'light' ? 'dark' : 'light');
  }

  sidebarpositionChange(){
    localStorage.setItem('sidebarpos',this.options.sidebarpos == 'fixed' ? 'absolute' : 'fixed');
    return (this.options.sidebarpos == 'fixed' ? 'absolute' : 'fixed');
  }

  headerposChange(){
    localStorage.setItem('headerpos',this.options.headerpos == 'fixed' ? 'absolute' : 'fixed');
    return (this.options.headerpos == 'fixed' ? 'absolute' : 'fixed');
  }


  dirChange(){
    localStorage.setItem('dir',this.options.dir == 'rtl' ? 'ltr' : 'rtl');
    return (this.options.dir == 'rtl' ? 'ltr' : 'rtl');
  }

  sidebarchange(sidebar){
    localStorage.setItem('sidebar',sidebar);
    return sidebar;
  }

  changeLogoBg(logobg){
    localStorage.setItem('logobg',logobg);
    return logobg;
  }

  changeNavBar(navbar){
    localStorage.setItem('navbar',navbar);
    return navbar;
  }


  changesidebar(sidebarbg){
    localStorage.setItem('sidebarbg',sidebarbg);
    return sidebarbg;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: string) {
    this.handleSidebar();
  }

  handleSidebar() {
    this.innerWidth = window.innerWidth;
    switch (this.defaultSidebar) {
      case 'full':
      case 'iconbar':
        if (this.innerWidth < 1170) {
          this.options.sidebartype = 'mini-sidebar';
        } else {
          this.options.sidebartype = this.defaultSidebar;
        }
        break;

      case 'overlay':
        if (this.innerWidth < 767) {
          this.options.sidebartype = 'mini-sidebar';
        } else {
          this.options.sidebartype = this.defaultSidebar;
        }
        break;

      default:
    }
  }



  toggleThemeSettingsDisplay() {
    setTimeout(() => {
			this.showSettings = !this.showSettings;
		}, 100);
  }

  toggleSidebarType() {

    switch (this.options.sidebartype) {
      case 'full':
      case 'iconbar':
        this.options.sidebartype = 'mini-sidebar';
        break;

      case 'overlay':
        this.showMobileMenu = !this.showMobileMenu;
        break;

      case 'mini-sidebar':
        if (this.defaultSidebar === 'mini-sidebar') {
          this.options.sidebartype = 'full';
        } else {
          this.options.sidebartype = this.defaultSidebar;
        }
        break;

      default:
    }
  }


}
