
import { Routes } from '@angular/router';

import { FullComponent } from './layouts/full/full.component';
import { BlankComponent } from './layouts/blank/blank.component';
import { AuthGuard } from './core/guards/auth.guard';

export const Approutes: Routes = [
    {
        path: '',
        component: FullComponent,
        data: {
          type: 'FullComponent'
        },
        canActivate: [AuthGuard],
        children: [
            { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
            {
                path: 'dashboard',
                loadChildren: () => import('./dashboards/dashboard.module').then(m => m.DashboardModule)
            },
            {
              path: 'users',
              loadChildren: () => import('./modules/user/user.module').then(m => m.UserModule)
            },
            {
              path: 'organization',
              loadChildren: () => import('./modules/organization/organization.module').then(m => m.OrganizationModule)
            },
            {
              path: 'department',
              loadChildren: () => import('./modules/department/department.module').then(m => m.DepartmentModule)
            },
            {
              path: 'employee',
              loadChildren: () => import('./modules/employee/employee.module').then(m => m.EmployeeModule)
            },
        ]
    },
    {
      path: '',
      component: BlankComponent,
      data: {
        type: 'BlankComponent'
      },
      canActivate: [AuthGuard],
      children: [
          {
              path: 'auth',
              loadChildren:
                  () => import('./modules/auth/auth.module').then(m => m.AuthModule)
          }
      ]
    },

    {
        path: '**',
        redirectTo: '/auth/login'
    }
];
