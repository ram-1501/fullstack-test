import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";

import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ChartsModule } from "ng2-charts";
import { ChartistModule } from "ng-chartist";
import { NgxChartsModule } from "@swimlane/ngx-charts";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { CalendarModule, DateAdapter } from "angular-calendar";

import { adapterFactory } from "angular-calendar/date-adapters/date-fns";
import { DashboardRoutes } from "./dashboard.routing";

import { Dashboard2Component } from "./dashboard2/dashboard2.component";
import { NgApexchartsModule } from "ng-apexcharts";
import { InfocardComponent } from "./dashboard-components/info-card/info-card.component";



@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    NgbModule,
    ChartsModule,
    ChartistModule,
    RouterModule.forChild(DashboardRoutes),
    PerfectScrollbarModule,

  ],
  declarations: [
    Dashboard2Component,
    InfocardComponent
  ],
})
export class DashboardModule { }
