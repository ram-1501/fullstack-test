import { Component, OnInit, ViewChild } from '@angular/core';
import { DepartmentService } from 'src/app/modules/department/department.service';
import { EmployeeService } from 'src/app/modules/employee/employee.service';
import { OrganizationService } from 'src/app/modules/organization/organization.service';



@Component({
  selector: 'app-info-card',
  templateUrl: './info-card.component.html',
  styleUrls: ['./info-card.component.css']
})
export class InfocardComponent implements OnInit{
  org = 0;
  dept = 0;
  emp = 0;
  constructor(
    private organizationService: OrganizationService,
    private departmentService: DepartmentService,
    private employeeService: EmployeeService
    ) {}


  ngOnInit() {
    this.organizationService.count().subscribe((count: any) => {
      this.org = count;
    });

    this.departmentService.count().subscribe((count: any) => {
      this.dept = count;
    });

    this.employeeService.count().subscribe((count: any) => {
      this.emp = count;
    });
  }


}

