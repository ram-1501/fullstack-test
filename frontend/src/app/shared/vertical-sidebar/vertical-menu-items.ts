import { RouteInfo } from './vertical-sidebar.metadata';


export const ROUTES: RouteInfo[] = [
  {
      path: '/dashboard',
      title: 'Dashboard',
      icon: 'Home',
      class: '',
      extralink: false,
      label: '',
      labelClass: 'badge badge-success sidebar-badge',
      submenu: []
  },
  {
    path: '/organization',
    title: 'Organization',
    icon: 'Layout',
    class: '',
    extralink: false,
    label: '',
    labelClass: '',
    submenu: []
  },

  {
    path: '/department',
    title: 'Departments',
    icon: 'Layout',
    class: '',
    extralink: false,
    label: '',
    labelClass: '',
    submenu: []
  },
  {
    path: '/employee',
    title: 'Employees',
    icon: 'Users',
    class: '',
    extralink: false,
    label: '',
    labelClass: '',
    submenu: []
  }
];
