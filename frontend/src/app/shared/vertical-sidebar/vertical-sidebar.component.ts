import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { UserSettingsInterface } from 'src/app/core/interfaces/user-settings.interface';
import { AppSettingsService } from 'src/app/core/services/app-settings.service';
import { RouteInfo } from './vertical-sidebar.metadata';
import { VerticalSidebarService } from './vertical-sidebar.service';


@Component({
  selector: 'app-vertical-sidebar',
  templateUrl: './vertical-sidebar.component.html'
})
export class VerticalSidebarComponent {
  showMenu = '';
  showSubMenu = '';
  public sidebarnavItems: RouteInfo[] = [];
  path = '';

  private _destroyed$ = new Subject();
  public userData: UserSettingsInterface;
  constructor(
    private menuServise: VerticalSidebarService,
    private router: Router,
    private appSettingsService: AppSettingsService
    ) {

    this.menuServise.items.subscribe(menuItems => {
      console.log('userSettingsSource', this.appSettingsService.userSettingsSource.value);

      this.sidebarnavItems = menuItems;

      // Active menu
      this.sidebarnavItems.filter(m => m.submenu.filter(
        (s) => {
          if (s.path === this.router.url) {
            this.path = m.title;
          }
        }
      ));
      this.addExpandClass(this.path);
    });

    this.appSettingsService.userSettings$
      .pipe(
        takeUntil(this._destroyed$)
      )
      .subscribe((userData) => {
        if (userData) {
          this.userData = userData;
        }
      });
  }

  addExpandClass(element: any) {
    if (element === this.showMenu) {
      this.showMenu = '0';
    } else {
      this.showMenu = element;
    }
  }

  addActiveClass(element: any) {
    if (element === this.showSubMenu) {
      this.showSubMenu = '0';
    } else {
      this.showSubMenu = element;
    }
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }

  logout() {
    this.appSettingsService.sessionExpire().then(() => {
      // this.router.navigate(['auth/login'], { queryParams: {} });
      location.href = location.protocol + '//' + location.host + '/auth';
    });
  }

}
