import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { BehaviorSubject, forkJoin, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpCancelService } from './httpcancel.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserSettingsInterface } from '../interfaces/user-settings.interface';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class AppSettingsService {
  public tokenName = 'fde-token';

  // source for observable
  public userSettingsSource: BehaviorSubject<any> = new BehaviorSubject(null);
  // observable stream
  public userSettings$ = this.userSettingsSource.asObservable();

  // source for observable
  public themeSettingsSource: BehaviorSubject<any> = new BehaviorSubject(null);
  // observable stream
  public themeSettings$ = this.themeSettingsSource.asObservable();

  public roles = [];
  constructor(
    private http: HttpClient,
    protected localStorage: LocalStorage,
    private httpCancelService: HttpCancelService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) { }

  setUserSettings(data: any) {
    console.log('setUserSettings', data);
    this.userSettingsSource.next(data);
  }

  setUserToken(token: string) {
    return this.localStorage.setItem(this.tokenName, token);
  }


  getUserByToken() {
    return this.http.get('user/me').pipe(
      map((resp: any) => {
        if (!resp.status) {
          return null;
        }
        let rawUser = resp.data || {};
        let mappedUser: UserSettingsInterface = {
          id: rawUser.id,
          firstName: rawUser.firstName,
          lastName: rawUser.lastName,
          email: rawUser.email,
          active: rawUser.active,
          roles: []
        };
        return mappedUser
      }),
      map((user: UserSettingsInterface) => {
        this.setUserSettings(user);
        return user;
      })
    );
  }

  /**
   * getLoggedinUserId
   */
   public getLoggedinUserId(): boolean {
    let id = null;
    if (this.userSettingsSource.value && this.userSettingsSource.value.roleId) {
      id = this.userSettingsSource.value.id;
    }
    return id;
  }

  /**
   * getLoggedinUserData
   */
  public getLoggedinUserData() {
    let user = null;
    if (this.userSettingsSource.value) {
      user = this.userSettingsSource.value;
    }
    return user;
  }

  /**
   * getRoles
   */
  public getRoles() {
    if (this.roles.length) {
      return of(this.roles);
    }

    return this.http.get('role/list').pipe(
      map((resp: any) => {
        this.roles = resp.data || [];
        return this.roles;
      })
    )
  }
  /**
   * showLoader
   */
   public showLoader() {
    this.spinner.show();
  }

  /**
   * hideLoader
   */
  public hideLoader() {
    this.spinner.hide();
  }

  showSuccess(message = '', title = 'Success!') {
    this.toastr.success(message, title);
  }


  showError(message = '', title = 'Oops!') {
    this.toastr.error(message, title);
  }

  showWarning(message = '', title = 'Alert!') {
    this.toastr.warning(message, title);
  }

  showInfo(message = '') {
    this.toastr.info(message);
  }

  public confirm(
    title: string,
    message: string,
    btnOkText: string = '',
    btnCancelText: string = '',
    hideOkButton: boolean = false) {

    btnOkText = (btnOkText) ? btnOkText : "OK";
    btnCancelText = (btnCancelText) ? btnCancelText : "Cancel";

    const flag = confirm(message);


  }


  public sessionExpire() {
    return new Promise((resolve) => {
      let observables = [
        this.localStorage.removeItem(this.tokenName)
      ];
      forkJoin(observables)
        .subscribe((response: any) => {
          this.httpCancelService.cancelPendingRequests();
          return resolve(true);
        }, (error) => { return resolve(false); });
    })
  }
}
