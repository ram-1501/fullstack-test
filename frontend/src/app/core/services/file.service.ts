import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class FileService {
  private readonly path = "file";

  constructor(private readonly http: HttpClient) {
    
  }

  public uploadFile(folder: string, file: File): Observable<string> {
    const formData: FormData = new FormData();
    formData.append("file", file);

    return this.http.post(this.path + '/' + folder, formData, { responseType: "text" });
  }
}
