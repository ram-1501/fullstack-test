import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { AppSettingsService } from '../services/app-settings.service';
import { mergeMap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private localStorage: LocalStorage,
    private appSettingsService: AppSettingsService
  ) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    return new Promise((resolve, reject) => {
      this.localStorage.getItem(this.appSettingsService.tokenName).pipe(
        mergeMap((token: any) => {
          console.log('TOKEN::', token);
          if (token) {
            return this.appSettingsService.getUserByToken();
          }
          return of(null);
        }),
        map((userData: any) => {
          console.log('userData', userData, next.data);
          if (userData) {
            if (next.data.type === 'BlankComponent') {
              this.router.navigate(['dashboard'], { queryParams: {} });
              return resolve(false);
            }
            return resolve(true);
          } else {
            if (next.data.type === 'BlankComponent') {
              return resolve(true);
            }
            this.router.navigate(['/auth/login'], { queryParams: {} });
            return resolve(false);
          }
        })
      ).subscribe();
    });
  }

}
