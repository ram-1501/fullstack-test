import { mergeMap, map } from 'rxjs/operators';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { AppSettingsService } from './../services/app-settings.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {

  constructor(
    private router: Router,
    private localStorage: LocalStorage,
    private appSettingsService: AppSettingsService
  ) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    let userData = this.appSettingsService.getLoggedinUserData();
    // console.log('RoleGuard', next, userData);
    if (userData && userData.roleId && next.data && Array.isArray(next.data.role)) {
      if (next.data.role.indexOf(userData.roleId) === -1) {

        switch (userData.roleId) {
          case 1:
            this.router.navigate(['admin'], { queryParams: {} });
            break;
          case 2:
            this.router.navigate(['providers/dashboard'], { queryParams: {} });
            break;
          case 3:
            this.router.navigate(['patient/dashboard'], { queryParams: {} });
            break;
          default:
            break;
        }
        return false;
      }
    }


    return true;
  }

}
