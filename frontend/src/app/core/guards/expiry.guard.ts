import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AppSettingsService } from '../services/app-settings.service';

@Injectable({
  providedIn: 'root'
})
export class ExpiryGuard implements CanActivate {
  constructor(
    private router: Router, 
    private appSettingsService:AppSettingsService) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let userData = this.appSettingsService.getLoggedinUserData();
    console.log('ExpiryGuard::called');
    

    if (userData && !userData.provider.isVerified) {
      this.router.navigate(['providers/dashboard'], { queryParams: {} });
      return false;
    }
    console.log('ExpiryGuard::userData',userData);
    return true;
  }

}
