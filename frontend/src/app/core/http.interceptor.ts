import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError, from } from 'rxjs';
import { environment } from 'src/environments/environment';
import { LocalStorage } from "@ngx-pwa/local-storage";
import { takeUntil, timeout, map, catchError, switchMap } from 'rxjs/operators';
import { NgxSpinnerService } from "ngx-spinner";
import { HttpCancelService } from './services/httpcancel.service';
import { AppSettingsService } from './services/app-settings.service';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {
  private baseURL = environment.apiUrl;

  private jwtToken: any;
  private openURLs: Array<string> = ["/auth/login", "/auth/create", "/auth/forgotPassword", "/auth/resetPassword"];
  private defaultTimeout: number = 60;

  constructor(
    private localStorage: LocalStorage,
    private spinner: NgxSpinnerService,
    private httpCancelService: HttpCancelService,
    private appSettingsService: AppSettingsService
  ) { }

  intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    if (this.openURLs.indexOf(req.url) > -1) {
      this.jwtToken = undefined;
    };

    let showLoader = false;
    if (req.headers.has('showLoader')) {
      showLoader = true;
      this.spinner.show();
    } else {
      req = req.clone({
        headers: req.headers.delete('showLoader')
      });
    }

    if (this.jwtToken) {
      return this.prepareUrlAndHeaders(req, next, showLoader);
    }

    return from(this.localStorage.getItem(this.appSettingsService.tokenName))
      .pipe(
        switchMap((jwtToken) => {
          this.jwtToken = jwtToken;
          return this.prepareUrlAndHeaders(req, next, showLoader);
        })
      );
  }

  private prepareUrlAndHeaders(req: HttpRequest<any>, next: HttpHandler, showLoader: boolean = false): Observable<HttpEvent<any>> {
    let baseUrl = this.baseURL;
    if (req.url.indexOf('i18n') !== -1 || (req.url.indexOf('http://') === 0 || req.url.indexOf('https://') === 0)) {
      baseUrl = '';
    }

    req = req.clone({
      url: baseUrl + req.url,
      setHeaders: {
        Authorization: `Bearer ${this.jwtToken}`
      }
    });

    //Make request
    return next
      .handle(req)
      .pipe(
        takeUntil(this.httpCancelService.onCancelPendingRequests()),
        timeout(1000 * this.defaultTimeout),
        map((event) => {
          if (event instanceof HttpResponse) {
            if (showLoader) {
              setTimeout(() => {
                /** spinner ends after 0.2 seconds */
                this.spinner.hide();
              }, 1000 * 0.2);
            }

            //check session
            if (event.body && (event.body.sessionExpired || event.body.returnCode == 'WRONG_PASSWORD')) {
              // this.userSettingsService.sessionExpire().then((res: any) => {
              //   this.localStorage.setItem('sessionExpired', 'true').subscribe(() => {
              //     window.location.href = window.location.href;
              //   });

              // });
            }
          }
          return event;
        }),
        catchError((error: HttpErrorResponse) => {
          console.log("httperror", error);
          if (error.status == 401) {
            this.appSettingsService.sessionExpire().then((flag) => {
              console.log(flag);
              if (flag) {
                window.location.href = '/auth/login';
              }
            });
          }

          if (showLoader) {
            this.spinner.hide();
          }

          return throwError(error);
        })
      );
  }
}
