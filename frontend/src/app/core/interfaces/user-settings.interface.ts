export interface UserSettingsInterface {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  active: number;
  roles: Array<any>;
}
