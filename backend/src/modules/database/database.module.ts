import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { SequelizeModule } from '@nestjs/sequelize';

@Module({
    imports: [
        SequelizeModule.forRootAsync({
            // name: 'flnv',
            useFactory: () => {
                const dbConfig = new ConfigService();
                return {
                    dialect: 'mysql',
                    host: dbConfig.get('DATABASE_HOST'),
                    port: +dbConfig.get('DATABASE_PORT'),
                    username: dbConfig.get('DATABASE_USER'),
                    password: dbConfig.get('DATABASE_PASSWORD'),
                    database: dbConfig.get('DATABASE_NAME'),
                    define: {
                        underscored: false
                    },
                    autoLoadModels: true,
                    synchronize: true,
                }
            }
        })
    ],
    providers: []
})
export class DatabaseModule { }
