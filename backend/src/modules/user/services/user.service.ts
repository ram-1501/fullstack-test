import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { CreateUserDto } from '../dto/create-user.dto';
import { Role } from '../models/role.model';
import { UserRole } from '../models/user-role.model';
import { User } from '../models/user.model';
import * as bcrypt from 'bcrypt';
import { ListQueryParamsDto } from 'src/modules/common/dto/list-query.dto';
import { Sequelize } from 'sequelize-typescript';
import { Op } from 'sequelize';
@Injectable()
export class UserService {
    constructor(
        @InjectModel(User) private readonly userModel: typeof User,
        @InjectModel(UserRole) private readonly userRoleModel: typeof UserRole,
        private readonly sequelize: Sequelize,
    ) { }

    findOne(where: Record<any, any>): Promise<User> {
        return this.userModel.findOne({ where: where })
    }

    getUser(userId: number): Promise<User> {
        return this.userModel.findOne({
            where: { id: userId },
            attributes: { exclude: ['password'] },
            include: [
                {
                    model: UserRole,
                    include: [Role]
                }
            ]
        })
    }

    async create(userData: CreateUserDto) {
        let transaction;
        try {
            transaction = await this.sequelize.transaction();

            const hashedPassword = await bcrypt.hash(userData.password, 10);

            let data = {
                password: hashedPassword,
                email: userData.email,
                firstName: userData.firstName,
                lastName: userData.lastName,
                active: typeof userData.active == 'number' ? userData.active : 1
            };

            const user = await this.userModel.create(data, { transaction });

            await this.userRoleModel.create({
                userId: user.id,
                roleId: userData.roleId
            }, { transaction });

            await transaction.commit();

            return await this.getUser(user.id);

        } catch (error) {
            if (transaction) await transaction.rollback();
            return null;
        }
    }

    async update(userId: number, userData: CreateUserDto) {
        let transaction;
        try {
            transaction = await this.sequelize.transaction();

            const data: any = {};

            if (typeof userData.email == 'string') {
                data.email = userData.email;
            }

            if (typeof userData.firstName == 'string') {
                data.userName = userData.firstName;
            }

            if (typeof userData.lastName == 'string') {
                data.lastName = userData.lastName;
            }

            if (typeof userData.active == 'number') {
                data.active = userData.active;
            }

            if (userData.password != '******') {
                data.password = await bcrypt.hash(userData.password, 10);
            }

            await this.userModel.update(data, { where: { id: userId }, transaction });

            // const user = await this.getUser(userId);

            await this.userRoleModel.destroy({ where: { userId: userId }, transaction });

            this.userRoleModel.create({
                userId: userId,
                roleId: userData.roleId
            }, { transaction });

            await transaction.commit();

            return await this.getUser(userId);

        } catch (error) {
            if (transaction) await transaction.rollback();
            return null;
        }
    }

    async list(queryParams: ListQueryParamsDto) {
        const searchText = queryParams.queryString || '';

        queryParams.pageNumber = queryParams.pageNumber || 0;
        queryParams.pageSize = queryParams.pageSize || 10;

        const offset = queryParams.pageNumber * queryParams.pageSize;
        const limit = queryParams.pageSize;
        const sortField = queryParams.sortField || 'id';
        const sortOrder = queryParams.sortOrder || 'desc';

        const where = {};
        if (queryParams.filter) {
            if (queryParams.filter.firstName) {
                where['firstName'] = queryParams.filter.firstName;
            }

            if (queryParams.filter.lastName) {
                where['lastName'] = queryParams.filter.lastName;
            }

        }


        let orderBy: any = [['id', 'desc']];
        // if (sortField == 'id') {
        //     orderBy = [[sortField, sortOrder]];
        // } else if (queryParams.sortField == 'name') {
        //     orderBy = [[Sequelize.literal('`providerRegistrationModel.firstName`'), sortOrder]]
        // }

        return await this.userModel.findAndCountAll({
            distinct: true,
            where: {
                [Op.or]: [
                    {
                        firstName: { [Op.like]: '%' + searchText + '%' }
                    },
                    {
                        lastName: { [Op.like]: '%' + searchText + '%' }
                    },
                    {
                        email: { [Op.like]: '%' + searchText + '%' }
                    }
                ]
            },
            offset: offset,
            limit: limit,
            order: orderBy
        });
    }

    deleteUser(where: Record<any, any>): Promise<any> {
        return this.userModel.destroy({ where: where })
    }
}
