import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { CreateRoleDto } from '../dto/create-role.dto';
import { Role } from '../models/role.model';

@Injectable()
export class RoleService {
    constructor(
        @InjectModel(Role) private readonly roleModel: typeof Role,
    ) { }

    findOne(where: Record<any, any>): Promise<Role> {
        return this.roleModel.findOne({ where: where })
    }

    findAll(): Promise<Role[]> {
        return this.roleModel.findAll();
    }

    async create(roleData: CreateRoleDto) {
        try {
            let data = {
                name: roleData.name
            };
            const role = await this.roleModel.create(data);
            return await this.findOne({ id: role.id });

        } catch (error) {
            return null;
        }
    }
}
