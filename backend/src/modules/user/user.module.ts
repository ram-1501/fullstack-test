import { Module } from '@nestjs/common';
import { UserController } from './controllers/user.controller';
import { UserService } from './services/user.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { User } from './models/user.model';
import { Role } from './models/role.model';
import { UserRole } from './models/user-role.model';
import { RoleController } from './controllers/role.controller';
import { RoleService } from './services/role.service';

@Module({
  imports: [
    SequelizeModule.forFeature([
      User,
      Role,
      UserRole
    ])
  ],
  controllers: [UserController, RoleController],
  providers: [UserService, RoleService],
  exports: [UserService]
})
export class UserModule { }
