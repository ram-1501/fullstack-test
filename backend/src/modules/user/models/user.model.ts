import { Table, Model, Column, HasMany, BelongsToMany, BelongsTo, HasOne } from "sequelize-typescript";
import { Role } from "./role.model";
import { UserRole } from "./user-role.model";

@Table
export class User extends Model {

    @Column
    email: string;

    @Column
    password: string;

    @Column
    firstName: string;

    @Column
    lastName: string;

    @Column
    active: number;

    @HasMany(() => UserRole)
    userRoles: UserRole[]

    @BelongsToMany(() => Role, () => UserRole)
    roles: Array<Role & { userRole: UserRole }>;
}