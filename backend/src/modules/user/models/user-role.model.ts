import { Table, Model, ForeignKey, Column, BelongsTo } from "sequelize-typescript";
import { Role } from "./role.model";
import { User } from "./user.model";

@Table
export class UserRole extends Model {
    @ForeignKey(() => User)
    @Column
    userId: number;

    @ForeignKey(() => Role)
    @Column
    roleId: number;

    @BelongsTo(() => User, {
        // onUpdate: "CASCADE",
        onDelete: "CASCADE",
        // hooks: true
    })
    user: User;

    @BelongsTo(() => Role, {
        // onUpdate: "CASCADE",
        onDelete: "CASCADE",
        // hooks: true
    })
    role: Role;
}
