import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";

export class CreateUserDto {

    @ApiProperty({ type: String, default: '' })
    @IsString()
    @IsNotEmpty()
    password: string;

    @ApiProperty({ type: String, default: '' })
    @IsString()
    @IsNotEmpty()
    email: string;

    @ApiProperty({ type: String, default: '' })
    @IsString()
    @IsNotEmpty()
    firstName: string;

    @ApiProperty({ type: String, default: '', required: false })
    @IsString()
    @IsOptional()
    lastName?: string;

    @ApiProperty({ type: Number, default: '', required: false })
    @IsNumber()
    @IsOptional()
    active?: number;

    @ApiProperty({ type: Number, default: '' })
    @IsNumber()
    @IsNotEmpty()
    roleId: number;
}

