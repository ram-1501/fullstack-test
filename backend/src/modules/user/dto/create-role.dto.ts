import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";

export class CreateRoleDto {
    @ApiProperty({ type: String, default: '' })
    @IsString()
    @IsNotEmpty()
    name: string;
}