import { Body, Controller, Get, Param, Post, Put, ParseIntPipe, Patch, Delete } from '@nestjs/common';
import { ApiBody, ApiCreatedResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Auth } from 'src/modules/common/decorators/auth.decorator';
import { UserData } from 'src/modules/common/decorators/user.decorator';
import { ListQueryParamsDto } from 'src/modules/common/dto/list-query.dto';
import { ResponseData } from 'src/modules/common/response-data';
import { CreateUserDto } from '../dto/create-user.dto';
import { UserService } from '../services/user.service';

@Controller('user')
@ApiTags('User')
@Auth()
export class UserController {
    constructor(
        private userService: UserService
    ) { }

    @Get('me')
    async getUserByToken(@UserData() user): Promise<any> {
        const output = new ResponseData();
        try {
            output.data = user;
        } catch (error) {
            output.status = false;
            output.message = typeof error == 'string' ? error : '';
        }
        return output;
    }

    @ApiOperation({ summary: "List users" })
    @ApiBody({ type: ListQueryParamsDto })
    @ApiCreatedResponse({
        description: 'resturn users data.',
        type: ResponseData,
    })
    @Post('list')
    async listUser(@Body() queryParams: ListQueryParamsDto) {
        const output = new ResponseData();
        try {
            output.data = await this.userService.list(queryParams);
        } catch (error) {
            console.log(error);
            output.status = false;
            output.message = typeof error == 'string' ? error : '';
        }
        return output;
    }

    @ApiOperation({ summary: "Get User By Id" })
    @ApiCreatedResponse({
        description: 'return users data.',
        type: ResponseData,
    })
    @Get(':id')
    async getUser(@Param('id', ParseIntPipe) userId: number) {

        const output = new ResponseData();
        try {
            const user = await this.userService.getUser(userId);
            if (!user) {
                throw 'Invalid User Id.';
            }

            output.data = user;

        } catch (error) {
            console.log(error);
            output.status = false;
            output.message = typeof error == 'string' ? error : '';
        }
        return output;
    }

    @ApiOperation({ summary: "Create User" })
    @ApiBody({ type: CreateUserDto })
    @ApiCreatedResponse({
        description: 'User data.',
        type: ResponseData,
    })
    @Put()
    async createUser(@Body() userData: CreateUserDto) {
        const output = new ResponseData();

        try {
            const user = await this.userService.findOne({ email: userData.email });
            if (user) {
                throw "User already exists.";
            }

            const result = await this.userService.create(userData);

            if (!result) {
                throw 'something went wrong';
            }

            output.data = await this.userService.findOne({ email: userData.email });
        } catch (error) {
            output.status = false;
            output.message = typeof error == 'string' ? error : '';
        }

        return output;
    }

    @ApiOperation({ summary: "Update User" })
    @ApiBody({ type: CreateUserDto })
    @ApiCreatedResponse({
        description: 'User data.',
        type: ResponseData,
    })
    @Patch(':id')
    async updateUser(@Param('id', ParseIntPipe) userId: number, @Body() userData: CreateUserDto) {
        const output = new ResponseData();

        try {
            const user = await this.userService.findOne({ email: userData.email });
            if (!user) {
                throw "User does not exist.";
            }

            const result = await this.userService.create(userData);

            if (!result) {
                throw 'something went wrong';
            }

            output.data = result;
        } catch (error) {
            output.status = false;
            output.message = typeof error == 'string' ? error : '';
        }

        return output;
    }

    @ApiOperation({ summary: "Delete User" })
    @Delete(':id')
    async deleteUser(@Param('id', ParseIntPipe) userId: number) {
        const output = new ResponseData();

        try {
            const user = await this.userService.findOne({ id: userId });
            if (!user) {
                throw "User does not exist.";
            }

            const result = await this.userService.deleteUser({ id: user.id });

            if (!result) {
                throw 'something went wrong';
            }

            output.data = result;
        } catch (error) {
            output.status = false;
            output.message = typeof error == 'string' ? error : '';
        }

        return output;
    }
}
