import { Body, Controller, Get, Put } from '@nestjs/common';
import { ApiBody, ApiCreatedResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Auth } from 'src/modules/common/decorators/auth.decorator';
import { ResponseData } from 'src/modules/common/response-data';
import { CreateRoleDto } from '../dto/create-role.dto';
import { RoleService } from '../services/role.service';

@Controller('role')
@ApiTags('Role')
@Auth()
export class RoleController {
    constructor(
        private roleService: RoleService
    ) { }

    @ApiOperation({ summary: "List roles" })
    @ApiCreatedResponse({
        description: 'return roles data.',
        type: ResponseData,
    })
    @Get('list')
    async listUser() {
        const output = new ResponseData();
        try {
            output.data = await this.roleService.findAll();
        } catch (error) {
            console.log(error);
            output.status = false;
            output.message = typeof error == 'string' ? error : '';
        }
        return output;
    }

    @ApiOperation({ summary: "Create Role" })
    @ApiBody({ type: CreateRoleDto })
    @ApiCreatedResponse({
        description: 'Role data.',
        type: ResponseData,
    })
    @Put()
    async createUser(@Body() body: CreateRoleDto) {
        const output = new ResponseData();

        try {
            const role = await this.roleService.findOne({ name: body.name });
            if (role) {
                throw "Role already exists.";
            }

            const result = await this.roleService.create(body);

            if (!result) {
                throw 'something went wrong';
            }

            output.data = result;
        } catch (error) {
            output.status = false;
            output.message = typeof error == 'string' ? error : '';
        }

        return output;
    }
}
