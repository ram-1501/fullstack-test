
import { ApiProperty } from "@nestjs/swagger";
import { IsNumber, IsOptional, IsString } from "class-validator";

export class ListQueryParamsDto {
    @ApiProperty({ type: String, default: '', example: '' })
    @IsOptional()
    @IsString()
    queryString?: string;

    @ApiProperty({ type: Number, default: 1, example: 0 })
    @IsOptional()
    @IsNumber()
    pageNumber?: number;

    @ApiProperty({ type: Number, default: 10, example: 10 })
    @IsOptional()
    @IsNumber()
    pageSize?: number;

    @ApiProperty({ type: String, default: 'id', example: 'id' })
    @IsOptional()
    @IsString()
    sortField?: string;

    @ApiProperty({ type: String, default: 'desc', example: 'desc' })
    @IsOptional()
    @IsString()
    sortOrder?: string;

    @ApiProperty({ type: Object, default: {}, example: {} })
    @IsOptional()
    // @Type(() => ListFilterDto)    
    filter?: any;

}