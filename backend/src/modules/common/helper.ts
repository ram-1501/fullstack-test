
export const csvToJSON = async (csvFilePath: string) => {

    try {
        const csv = require('csvtojson')
        const jsonArray = await csv().fromFile(csvFilePath);
        return jsonArray;
    } catch (error) {
        return [];
    }
}

export const leftPad = (number, targetLength) => {
    let output = number + '';
    while (output.length < targetLength) {
        output = '0' + output;
    }
    return output;
}

export const htmlToPdf = (html, toFile) => {
    return new Promise((resolve, reject) => {
        const pdf = require('html-pdf');
        pdf.create(html, { height: "11.0in", width: "8.5in" }).toFile(toFile, function (err, res) {
            if (err) {
                console.log(err);
                resolve(null);
            }
            console.log(res); // { filename: '/app/businesscard.pdf' }
            resolve(res);
        });
    });

}

export const mergePdfs = (pdfs, toFile) => {
    return new Promise((resolve, reject) => {
        const merge = require('easy-pdf-merge');
        merge(pdfs, toFile, function (err) {
            if (err) {
                console.log(err)
                resolve(null)
            }
            console.log('Success');
            resolve(true)
        });
    });

}

