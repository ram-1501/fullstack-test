import { Injectable } from '@nestjs/common';
import { UserService } from '../user/services/user.service';

@Injectable()
export class AuthService {

    constructor(
        private readonly usersService: UserService
    ) { }

    async login(where: any): Promise<any> {
        return await this.usersService.findOne(where)
    }

    async getUser(userId: any): Promise<any> {
        return await this.usersService.getUser(userId);
    }

    async signup(data: any): Promise<any> {
        return await this.usersService.create(data);

    }
}
