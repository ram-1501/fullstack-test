import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";

export class LoginRequestData {
    @ApiProperty({ type: String, default: '' })
    @IsString()
    @IsNotEmpty()
    email: string;

    @ApiProperty({ type: String, default: '' })
    @IsString()
    @IsNotEmpty()
    password: string;
}