import { Body, Controller, Get, Post } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { ApiBody, ApiCreatedResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { ResponseData } from '../common/response-data';
import { AuthService } from './auth.service';
import { LoginRequestData } from './dto/login-request-data';
import * as bcrypt from 'bcrypt';
import { CreateUserDto } from '../user/dto/create-user.dto';
@Controller('auth')
@ApiTags('Auth')
export class AuthController {
    constructor(
        private configService: ConfigService,
        private authService: AuthService,
        private readonly jwtService: JwtService,
    ) { }

    @ApiOperation({ summary: "Login" })
    @ApiBody({ type: LoginRequestData })
    @ApiCreatedResponse({
        description: 'User data.',
        type: ResponseData,
    })
    @Post('login')
    async login(@Body() loginData: LoginRequestData) {

        let output = new ResponseData();

        try {
            let user = await this.authService.login({ email: loginData.email });

            if (!user) {
                throw "User does not exist.";
            }

            const isPasswordMatching = await bcrypt.compare(
                loginData.password,
                user.password
            );

            if (!isPasswordMatching) {
                throw "Incorrect password.";
            }

            output.data.token = this.jwtService.sign({ user: { id: user.id } });

            output.data.user = await this.authService.getUser(user.id);

        } catch (error) {
            console.log(error);
            output.status = false;
            output.message = typeof error == 'string' ? error : '';
        }

        return output;
    }

    @ApiOperation({ summary: "Signup" })
    @ApiBody({ type: LoginRequestData })
    @ApiCreatedResponse({
        description: 'User data.',
        type: ResponseData,
    })
    @Post('signup')
    async signup(@Body() signupData: CreateUserDto) {

        let output = new ResponseData();

        try {
            let user = await this.authService.login({ email: signupData.email });

            if (user) {
                throw "User already exist.";
            }

            user = await this.authService.signup(signupData);

            output.data.user = await this.authService.getUser(user.id);

        } catch (error) {
            console.log(error);
            output.status = false;
            output.message = typeof error == 'string' ? error : '';
        }

        return output;
    }
}
