/* eslint-disable prettier/prettier */
import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, Put } from '@nestjs/common';
import { ApiBody, ApiCreatedResponse, ApiOperation } from '@nestjs/swagger';
import { Auth } from 'src/modules/common/decorators/auth.decorator';
import { UserData } from 'src/modules/common/decorators/user.decorator';
import { ListQueryParamsDto } from 'src/modules/common/dto/list-query.dto';
import { ResponseData } from 'src/modules/common/response-data';
import { CreateUserDto } from 'src/modules/user/dto/create-user.dto';
import { EmployeeService } from '../../services/employee/employee.service';
@Controller('employee')
@Auth()
export class EmployeeController {

    constructor(
        private employeeService: EmployeeService
    ) { }

    @Get('count')
    async count(@UserData() loggedinUser: any) {

        const output = new ResponseData();
        try {
            const item = await this.employeeService.count({ userId: loggedinUser.id });
            output.data = item;

        } catch (error) {
            console.log(error);
            output.status = false;
            output.message = typeof error == 'string' ? error : '';
        }
        return output;
    }

    @ApiOperation({ summary: "Get Dept Id" })
    @ApiCreatedResponse({
        description: 'return Dept data.',
        type: ResponseData,
    })
    @Get(':id')
    async get(@Param('id', ParseIntPipe) id: number) {

        const output = new ResponseData();
        try {
            const item = await this.employeeService.findOne({ id: id });
            if (!item) {
                throw 'Invalid Id.';
            }

            output.data = item;

        } catch (error) {
            console.log(error);
            output.status = false;
            output.message = typeof error == 'string' ? error : '';
        }
        return output;
    }

    @ApiOperation({ summary: "List Depts" })
    @ApiBody({ type: ListQueryParamsDto })
    @ApiCreatedResponse({
        description: 'resturn Depts data.',
        type: ResponseData,
    })
    @Post('list')
    async list(@Body() queryParams: ListQueryParamsDto, @UserData() loggedinUser: any) {
        const output = new ResponseData();
        try {
            if (!queryParams.filter) {
                queryParams.filter = {};
            }
            queryParams.filter.userId = loggedinUser.id;

            output.data = await this.employeeService.list(queryParams);
        } catch (error) {
            console.log(error);
            output.status = false;
            output.message = typeof error == 'string' ? error : '';
        }
        return output;
    }

    @ApiOperation({ summary: "Create Dept" })
    @ApiBody({ type: CreateUserDto })
    @ApiCreatedResponse({
        description: 'Dept data.',
        type: ResponseData,
    })
    @Put()
    async create(@Body() params: any) {
        const output = new ResponseData();

        try {
            const item = await this.employeeService.findOne({ departmentId: params.departmentId, firstName: params.firstName });
            if (item) {
                throw "Dept already exists.";
            }

            const result = await this.employeeService.create(params);

            if (!result) {
                throw 'something went wrong';
            }

            output.data = result;
        } catch (error) {
            output.status = false;
            output.message = typeof error == 'string' ? error : '';
        }

        return output;
    }

    @ApiOperation({ summary: "Update Dept" })
    @ApiBody({ type: CreateUserDto })
    @ApiCreatedResponse({
        description: 'Dept data.',
        type: ResponseData,
    })
    @Patch(':id')
    async update(@Param('id', ParseIntPipe) id: number, @Body() params: any) {
        const output = new ResponseData();

        try {
            const user = await this.employeeService.findOne({ id: id });
            if (!user) {
                throw "Dept does not exist.";
            }

            const result = await this.employeeService.update(id, params);

            if (!result) {
                throw 'something went wrong';
            }

            output.data = result;
        } catch (error) {
            output.status = false;
            output.message = typeof error == 'string' ? error : '';
        }

        return output;
    }

    @ApiOperation({ summary: "Delete Dept" })
    @Delete(':id')
    async delete(@Param('id', ParseIntPipe) id: number) {
        const output = new ResponseData();

        try {
            const user = await this.employeeService.findOne({ id: id });
            if (!user) {
                throw "Dept does not exist.";
            }

            const result = await this.employeeService.delete({ id: user.id });

            if (!result) {
                throw 'something went wrong';
            }

            output.data = result;
        } catch (error) {
            output.status = false;
            output.message = typeof error == 'string' ? error : '';
        }

        return output;
    }

}

