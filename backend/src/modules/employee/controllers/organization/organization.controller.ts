/* eslint-disable prettier/prettier */
import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, Put } from '@nestjs/common';
import { ApiBody, ApiCreatedResponse, ApiOperation } from '@nestjs/swagger';
import { Auth } from 'src/modules/common/decorators/auth.decorator';
import { UserData } from 'src/modules/common/decorators/user.decorator';
import { ListQueryParamsDto } from 'src/modules/common/dto/list-query.dto';
import { ResponseData } from 'src/modules/common/response-data';
import { CreateUserDto } from 'src/modules/user/dto/create-user.dto';
import { OrganizationService } from '../../services/organization/organization.service';

@Controller('organization')
@Auth()
export class OrganizationController {

    constructor(
        private organizationService: OrganizationService
    ) { }

    @Get('count')
    async count(@UserData() loggedinUser: any) {

        const output = new ResponseData();
        try {
            const item = await this.organizationService.count({ userId: loggedinUser.id });
            output.data = item;

        } catch (error) {
            console.log(error);
            output.status = false;
            output.message = typeof error == 'string' ? error : '';
        }
        return output;
    }

    @ApiOperation({ summary: "Get Org By Id" })
    @ApiCreatedResponse({
        description: 'return Org data.',
        type: ResponseData,
    })
    @Get(':id')
    async get(@Param('id', ParseIntPipe) id: number) {

        const output = new ResponseData();
        try {
            const item = await this.organizationService.findOne({ id: id });
            if (!item) {
                throw 'Invalid Id.';
            }

            output.data = item;

        } catch (error) {
            console.log(error);
            output.status = false;
            output.message = typeof error == 'string' ? error : '';
        }
        return output;
    }

    @ApiOperation({ summary: "List users" })
    @ApiBody({ type: ListQueryParamsDto })
    @ApiCreatedResponse({
        description: 'resturn users data.',
        type: ResponseData,
    })
    @Post('list')
    async list(@Body() queryParams: ListQueryParamsDto, @UserData() loggedinUser: any) {
        const output = new ResponseData();
        try {
            if (!queryParams.filter) {
                queryParams.filter = {};
            }
            queryParams.filter.userId = loggedinUser.id;

            output.data = await this.organizationService.list(queryParams);
        } catch (error) {
            console.log(error);
            output.status = false;
            output.message = typeof error == 'string' ? error : '';
        }
        return output;
    }

    @ApiOperation({ summary: "Create Org" })
    @ApiBody({ type: CreateUserDto })
    @ApiCreatedResponse({
        description: 'Org data.',
        type: ResponseData,
    })
    @Put()
    async create(@Body() params: any, @UserData() loggedinUser: any) {
        const output = new ResponseData();

        try {
            const item = await this.organizationService.findOne({ name: params.name });
            if (item) {
                throw "User already exists.";
            }

            const result = await this.organizationService.create(params, loggedinUser);

            if (!result) {
                throw 'something went wrong';
            }

            output.data = result;
        } catch (error) {
            output.status = false;
            output.message = typeof error == 'string' ? error : '';
        }

        return output;
    }

    @ApiOperation({ summary: "Update Org" })
    @ApiBody({ type: CreateUserDto })
    @ApiCreatedResponse({
        description: 'Org data.',
        type: ResponseData,
    })
    @Patch(':id')
    async update(@Param('id', ParseIntPipe) id: number, @Body() params: any) {
        const output = new ResponseData();

        try {
            const user = await this.organizationService.findOne({ id: id });
            if (!user) {
                throw "Organization does not exist.";
            }

            const result = await this.organizationService.update(id, params);

            if (!result) {
                throw 'something went wrong';
            }

            output.data = result;
        } catch (error) {
            output.status = false;
            output.message = typeof error == 'string' ? error : '';
        }

        return output;
    }

    @ApiOperation({ summary: "Delete Org" })
    @Delete(':id')
    async delete(@Param('id', ParseIntPipe) id: number) {
        const output = new ResponseData();

        try {
            const user = await this.organizationService.findOne({ id: id });
            if (!user) {
                throw "Organization does not exist.";
            }

            const result = await this.organizationService.delete({ id: user.id });

            if (!result) {
                throw 'something went wrong';
            }

            output.data = result;
        } catch (error) {
            output.status = false;
            output.message = typeof error == 'string' ? error : '';
        }

        return output;
    }

}
