/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Sequelize } from 'sequelize-typescript';
import { ListQueryParamsDto } from 'src/modules/common/dto/list-query.dto';
import { Op } from 'sequelize';
import { Organization } from '../../models/organization.model';
import { Employee } from '../../models/employee.model';
import { Department } from '../../models/department.model';
import { User } from 'src/modules/user/models/user.model';

@Injectable()
export class EmployeeService {

    constructor(
        @InjectModel(Employee) private readonly employeeModel: typeof Employee,
        private readonly sequelize: Sequelize,
    ) { }

    findOne(where: Record<any, any>): Promise<any> {
        return this.employeeModel.findOne({ where: where })
    }

    count(where: Record<any, any>): Promise<any> {
        return this.employeeModel.count({
            include: [
                {
                    model: Organization,
                    where: where
                }
            ]
        })
    }

    async create(data: any) {
        let transaction;
        try {
            transaction = await this.sequelize.transaction();

            const idata = {
                organizationId: data.organizationId,
                departmentId: data.departmentId,
                firstName: data.firstName,
                lastName: data.lastName,
                dob: data.dob,
                workTitle: data.workTitle,
                experience: data.experience
            };

            const item = await this.employeeModel.create(idata, { transaction });

            await transaction.commit();

            return await this.findOne({ id: item.id });

        } catch (error) {
            if (transaction) await transaction.rollback();
            return null;
        }
    }

    async update(id: number, data: any) {
        let transaction;
        try {
            transaction = await this.sequelize.transaction();

            const udata: any = {};

            if (typeof data.firstName == 'string') {
                udata.firstName = data.firstName;
            }

            if (typeof data.lastName == 'string') {
                udata.lastName = data.lastName;
            }

            if (typeof data.dob == 'string') {
                udata.dob = data.dob;
            }

            if (typeof data.workTitle == 'string') {
                udata.workTitle = data.workTitle;
            }

            if (typeof data.experience == 'string') {
                udata.experience = data.experience;
            }

            if (typeof data.organizationId == 'string' || typeof data.organizationId == 'number') {
                udata.organizationId = data.organizationId;
            }

            if (typeof data.departmentId == 'string' || typeof data.departmentId == 'number') {
                udata.departmentId = data.departmentId;
            }

            await this.employeeModel.update(udata, { where: { id: id }, transaction });



            await transaction.commit();

            return await this.findOne({ id: id });

        } catch (error) {
            if (transaction) await transaction.rollback();
            return null;
        }
    }

    async list(queryParams: ListQueryParamsDto) {
        const searchText = queryParams.queryString || '';

        queryParams.pageNumber = queryParams.pageNumber || 0;
        queryParams.pageSize = queryParams.pageSize || 10;

        const offset = queryParams.pageNumber * queryParams.pageSize;
        const limit = queryParams.pageSize;
        const sortField = queryParams.sortField || 'id';
        const sortOrder = queryParams.sortOrder || 'desc';

        const where = {};

        let orderBy: any = [['id', 'desc']];
        if (sortField == 'id') {
            orderBy = [[sortField, sortOrder]];
        }

        return await this.employeeModel.findAndCountAll({
            distinct: true,
            where: {
                [Op.or]: [
                    {
                        firstName: { [Op.like]: '%' + searchText + '%' }
                    },
                    {
                        lastName: { [Op.like]: '%' + searchText + '%' }
                    },
                    {
                        dob: { [Op.like]: '%' + searchText + '%' }
                    },
                    {
                        workTitle: { [Op.like]: '%' + searchText + '%' }
                    },
                    {
                        experience: { [Op.like]: '%' + searchText + '%' }
                    }
                ]
            },
            include: [
                {
                    model: Organization,
                    required: true,
                    where: { userId: queryParams.filter.userId }
                },
                { model: Department }
            ],
            offset: offset,
            limit: limit,
            order: orderBy
        });
    }

    delete(where: Record<any, any>): Promise<any> {
        return this.employeeModel.destroy({ where: where })
    }
}


