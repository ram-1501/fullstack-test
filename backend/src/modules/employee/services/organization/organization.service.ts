/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Sequelize } from 'sequelize-typescript';
import { ListQueryParamsDto } from 'src/modules/common/dto/list-query.dto';
import { Organization } from '../../models/organization.model';
import { Op } from 'sequelize';

@Injectable()
export class OrganizationService {

    constructor(
        @InjectModel(Organization) private readonly organizationModel: typeof Organization,
        private readonly sequelize: Sequelize,
    ) { }

    findOne(where: Record<any, any>): Promise<any> {
        return this.organizationModel.findOne({ where: where })
    }

    count(where: Record<any, any>): Promise<any> {
        return this.organizationModel.count({ where: where })
    }

    async create(data: any, loggedinUser: any) {
        let transaction;
        try {
            transaction = await this.sequelize.transaction();

            const idata = {
                userId: loggedinUser.id,
                name: data.name,
                owner: data.owner,
                address: data.address,
                city: data.city,
                state: data.state,
                country: data.country
            };

            const item = await this.organizationModel.create(idata, { transaction });

            await transaction.commit();

            return await this.findOne({ id: item.id });

        } catch (error) {
            if (transaction) await transaction.rollback();
            return null;
        }
    }

    async update(id: number, data: any) {
        let transaction;
        try {
            transaction = await this.sequelize.transaction();

            const udata: any = {};

            if (typeof data.name == 'string') {
                udata.name = data.name;
            }

            if (typeof data.owner == 'string') {
                udata.owner = data.owner;
            }

            if (typeof data.address == 'string') {
                udata.address = data.address;
            }

            if (typeof data.state == 'string') {
                udata.state = data.state;
            }

            if (typeof data.city == 'string') {
                udata.city = data.city;
            }

            if (typeof data.country == 'string') {
                udata.country = data.country;
            }

            await this.organizationModel.update(udata, { where: { id: id }, transaction });



            await transaction.commit();

            return await this.findOne({ id: id });

        } catch (error) {
            if (transaction) await transaction.rollback();
            return null;
        }
    }

    async list(queryParams: ListQueryParamsDto) {
        const searchText = queryParams.queryString || '';

        queryParams.pageNumber = queryParams.pageNumber || 0;
        queryParams.pageSize = queryParams.pageSize || 10;

        const offset = queryParams.pageNumber * queryParams.pageSize;
        const limit = queryParams.pageSize;
        const sortField = queryParams.sortField || 'id';
        const sortOrder = queryParams.sortOrder || 'desc';


        let orderBy: any = [['id', 'desc']];
        if (sortField == 'id') {
            orderBy = [[sortField, sortOrder]];
        }

        return await this.organizationModel.findAndCountAll({
            distinct: true,
            where: {
                userId: queryParams.filter.userId,
                [Op.or]: [
                    {
                        name: { [Op.like]: '%' + searchText + '%' }
                    },
                    {
                        owner: { [Op.like]: '%' + searchText + '%' }
                    },
                    {
                        address: { [Op.like]: '%' + searchText + '%' }
                    },
                    {
                        city: { [Op.like]: '%' + searchText + '%' }
                    },
                    {
                        country: { [Op.like]: '%' + searchText + '%' }
                    }
                ]
            },
            offset: offset,
            limit: limit,
            order: orderBy
        });
    }

    delete(where: Record<any, any>): Promise<any> {
        return this.organizationModel.destroy({ where: where })
    }
}
