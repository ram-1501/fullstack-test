/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Sequelize } from 'sequelize-typescript';
import { ListQueryParamsDto } from 'src/modules/common/dto/list-query.dto';
import { Op } from 'sequelize';
import { Department } from '../../models/department.model';
import { Organization } from '../../models/organization.model';

@Injectable()
export class DepartmentService {

    constructor(
        @InjectModel(Department) private readonly departmentModel: typeof Department,
        private readonly sequelize: Sequelize,
    ) { }

    findOne(where: Record<any, any>): Promise<any> {
        return this.departmentModel.findOne({ where: where })
    }

    count(where: Record<any, any>): Promise<any> {
        return this.departmentModel.count({
            include: [
                {
                    model: Organization,
                    where: where
                }
            ]
        })
    }

    async create(data: any) {
        let transaction;
        try {
            transaction = await this.sequelize.transaction();

            const idata = {
                organizationId: data.organizationId,
                name: data.name,
                owner: data.owner,
                description: data.description,
                workingTime: data.workingTime,
                workingDays: data.workingDays
            };

            const item = await this.departmentModel.create(idata, { transaction });

            await transaction.commit();

            return await this.findOne({ id: item.id });

        } catch (error) {
            if (transaction) await transaction.rollback();
            return null;
        }
    }

    async update(id: number, data: any) {
        let transaction;
        try {
            transaction = await this.sequelize.transaction();

            const udata: any = {};

            if (typeof data.name == 'string') {
                udata.name = data.name;
            }

            if (typeof data.owner == 'string') {
                udata.owner = data.owner;
            }

            if (typeof data.description == 'string') {
                udata.description = data.description;
            }

            if (typeof data.workingTime == 'string') {
                udata.workingTime = data.workingTime;
            }

            if (typeof data.workingDays == 'string') {
                udata.workingDays = data.workingDays;
            }

            if (typeof data.organizationId == 'string' || typeof data.organizationId == 'number') {
                udata.organizationId = data.organizationId;
            }

            await this.departmentModel.update(udata, { where: { id: id }, transaction });



            await transaction.commit();

            return await this.findOne({ id: id });

        } catch (error) {
            if (transaction) await transaction.rollback();
            return null;
        }
    }

    async list(queryParams: ListQueryParamsDto) {
        const searchText = queryParams.queryString || '';

        queryParams.pageNumber = queryParams.pageNumber || 0;
        queryParams.pageSize = queryParams.pageSize || 10;

        const offset = queryParams.pageNumber * queryParams.pageSize;
        const limit = queryParams.pageSize;
        const sortField = queryParams.sortField || 'id';
        const sortOrder = queryParams.sortOrder || 'desc';

        const where = {};

        let orderBy: any = [['id', 'desc']];
        if (sortField == 'id') {
            orderBy = [[sortField, sortOrder]];
        }

        return await this.departmentModel.findAndCountAll({
            distinct: true,
            where: {
                [Op.or]: [
                    {
                        name: { [Op.like]: '%' + searchText + '%' }
                    },
                    {
                        owner: { [Op.like]: '%' + searchText + '%' }
                    },
                    {
                        description: { [Op.like]: '%' + searchText + '%' }
                    },
                    {
                        workingTime: { [Op.like]: '%' + searchText + '%' }
                    },
                    {
                        workingDays: { [Op.like]: '%' + searchText + '%' }
                    }
                ]
            },
            include: [
                {
                    model: Organization,
                    where: { userId: queryParams.filter.userId }
                }
            ],
            offset: offset,
            limit: limit,
            order: orderBy
        });
    }

    delete(where: Record<any, any>): Promise<any> {
        return this.departmentModel.destroy({ where: where })
    }
}

