/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { OrganizationController } from './controllers/organization/organization.controller';
import { DepartmentController } from './controllers/department/department.controller';
import { EmployeeController } from './controllers/employee/employee.controller';
import { EmployeeService } from './services/employee/employee.service';
import { DepartmentService } from './services/department/department.service';
import { OrganizationService } from './services/organization/organization.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { Organization } from './models/organization.model';
import { Department } from './models/department.model';
import { Employee } from './models/employee.model';

@Module({
  imports: [
    SequelizeModule.forFeature([
      Organization,
      Department,
      Employee
    ])
  ],
  controllers: [OrganizationController, DepartmentController, EmployeeController],
  providers: [EmployeeService, DepartmentService, OrganizationService]
})
export class EmployeeModule { }
