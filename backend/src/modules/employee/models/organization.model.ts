/* eslint-disable prettier/prettier */
import { Table, Model, Column, HasMany, BelongsToMany, BelongsTo, HasOne, ForeignKey } from "sequelize-typescript";
import { User } from "src/modules/user/models/user.model";

@Table
export class Organization extends Model {

    @ForeignKey(() => User)
    @Column
    userId: number;

    @Column
    name: string;

    @Column
    owner: string;

    @Column
    address: string;

    @Column
    city: string;

    @Column
    state: string;

    @Column
    country: string;

    @BelongsTo(() => User, {
        // onUpdate: "CASCADE",
        onDelete: "CASCADE",
        // hooks: true
    })
    user: User;
}