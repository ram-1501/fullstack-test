/* eslint-disable prettier/prettier */
import { Table, Model, Column, HasMany, BelongsToMany, BelongsTo, HasOne, ForeignKey } from "sequelize-typescript";
import { Organization } from "./organization.model";

@Table
export class Department extends Model {

    @ForeignKey(() => Organization)
    @Column
    organizationId: number;

    @Column
    name: string;

    @Column
    owner: string;

    @Column
    description: string;

    @Column
    workingTime: string;

    @Column
    workingDays: string;

    @BelongsTo(() => Organization, {
        // onUpdate: "CASCADE",
        onDelete: "CASCADE",
        // hooks: true
    })
    organization: Organization;
}