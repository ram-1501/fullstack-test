/* eslint-disable prettier/prettier */
import { Table, Model, Column, HasMany, BelongsToMany, BelongsTo, HasOne, ForeignKey } from "sequelize-typescript";
import { Department } from "./department.model";
import { Organization } from "./organization.model";

@Table
export class Employee extends Model {

    @ForeignKey(() => Organization)
    @Column
    organizationId: number;

    @ForeignKey(() => Department)
    @Column
    departmentId: number;

    @Column
    firstName: string;

    @Column
    lastName: string;

    @Column
    dob: string;

    @Column
    workTitle: string;

    @Column
    experience: string;

    @BelongsTo(() => Organization, {
        // onUpdate: "CASCADE",
        onDelete: "CASCADE",
        // hooks: true
    })
    organization: Organization;

    @BelongsTo(() => Department, {
        // onUpdate: "CASCADE",
        onDelete: "CASCADE",
        // hooks: true
    })
    department: Department;
}