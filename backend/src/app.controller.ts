import { Controller, Get, Header, Param, Res } from '@nestjs/common';
import { AppService } from './app.service';
import * as bcrypt from 'bcrypt';

import * as fs from 'fs';
import * as path from 'path';
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('reports/:fileId')
  async serveAvatar(@Param('fileId') fileId, @Res() res): Promise<any> {
    res.sendFile(fileId, { root: 'public/reports' });
  }


  @Get(':password')
  async getHello1(@Param('password') password: string) {
    return await bcrypt.hash(password, 10);
  }
}
